The experimental measurement of VBF $h \rightarrow ww \rightarrow \l \nu \l \nu$ production in the the e$\mu$ decay channel is reported as fiducial cross section defined as follows:

\begin{equation}
  \sigma^{\mathrm{fid}}_{\textrm{VBF}  h \rightarrow ww \rightarrow l \nu l \nu}
  = \frac{N_{\textrm{obs}} - N_{\textrm{bkg}}}{C \times {\cal{L}} },
\label{xSectFid}
\end{equation}

where ${\cal{L}}$ is the integrated luminosity, $N_{\textrm{obs}}$ is the observed number of events, $N_{\textrm{bkg}}$ is the estimated number of background events and $C$ is a factor that accounts for detector inefficiencies, resolution effects and contributions from $\tau$-lepton decays of the $W$ bosons.
The latter is defined as the ratio of the number of reconstructed signal events after the final selection with electrons or muons in the final state (including electrons or muons from $\tau$-decays) to the number of signal events generated in the fiducial region where only direct decays of $W$ bosons to electrons and muons are allowed.


The VBF $h \rightarrow ww \rightarrow \l \nu \l \nu$ cross section is evaluated in the fiducial phase space of the $e-\mu$ decay channel, as defined in Table \ref{table:fid_volume}. The definition of the fiducial phase space is chosen to be as close as possible to the kinematic phase space at detector level in order to minimize theoretical extrapolation and model dependence.

Electrons and muons are required at particle level to stem from one of the W bosons produced in the hard scatter and are defined at "dressed level", i.e. the momenta of photons emitted in a cone R = 0.1 around the lepton direction are added to the charged  lepton momentum after QED final-state radiation. Final-state particles, defined as those with a proper lifetime $\tau$ corresponding to $c\tau \ge 10$ mm, are clustered into jets (referred to as particle-level jets) using the same algorithm as for detector-level
jets, i.e. the anti-k${}_t$ algorithm with radius parameter R = 0.4.
The selected charged leptons and neutrinos from $W$ boson decays are not included in the jet clustering.
The fiducial phase space at particle level does not make any requirement on jets containing $b$-quarks.
The missing transverse momentum is defined at particle level as the transverse component of the vectorial sum of non-interacting particles. Its magnitude is denoted in Table \ref{table:fid_volume} as met
If a jet is within $\Delta$R=0.4 from the selected electron or muon, the event is not considered.


%Fiducial Volume
\begin{table}[!ht]
        \centering
        \begin{tabular}{ c |  c |  c}
        \hline\hline
        Category & Fiducial Requirement & Cut Value   \\
        \hline\hline
        		   	   	& $|Eta|$                      &   $<$ 2.5  \\		
					& $P_{T}$lead      			&   $> 22 GeV$   \\
           Leptons ($e+\mu$, $\mu+e$)  	& $P_{T}$sublead   			&   $> 15 GeV$  \\
	   	   			
	   	   			&  OS and OF pair     		&  Yes       \\
					& $\Delta$R($\ell,\ell$)       &   $> 0.1$ \\
           	   			& $mll$                  	&   $> 10 GeV$  \\
        \hline
        Neutrinos  			& $met$                        &   $> 20 GeV$   \\
	\hline
		   			& pTj 				&   $> 30 GeV$  \\
	                & |Etaj| 		        &   $< 4.5$  \\
   Jets (anti-k${}_t$ R=0.4)& $N_{\textrm{jets}}$           &   $\ge 2$      \\	
                    & $N_{\textrm{b-jet}}$           &   $< 1$      \\
                    & $M_{jj}$                      & $> 200 GeV$ \\
                    & $DY_{jj}$                      & $> 2.1$  \\
	\hline
	 lepton-jet separation		& $\Delta$R($\ell$,jet) 		&   $> 0.4$ \\
	\hline
	VBF	   			& OLV                           &   Yes   \\
          	   			& CJV                           &   Yes   \\
        \hline
        \hline
        \end{tabular}
        \caption{Definition of Fiducial Phase Space for VBF $h \rightarrow ww \rightarrow \l \nu \l\nu$  in the $e-\mu$ decay channel.}
        \label{table:fid_volume}
        \end{table}



The fiducial cross section is subsequently extrapolated in a broader phase space, referred to as the "inclusive" phase space in the following. An acceptance correction factor $A$ is used to extrapolate the fiducial cross section to an inclusive cross section, according to the following formula:

\begin{equation}
  \sigma^{\mathrm{incl}}_{VBF}
  = \frac{\sigma^{\mathrm{fid}}_{\textrm{VBF}h\rightarrow ww \rightarrow l\nu l \nu}}{A},
\label{xSectIncl}
\end{equation}

Theoretical calculations are used to estimate the acceptance correction $A$ from the fiducial to the inclusive phase space, and the extrapolated cross section is referred to in the following as inclusive cross section. The definition of the inclusive phase space is summarized in Table \ref{table:incl_volume}. The inclusive cross section is extracted for Higgs production in the vector boson fusion mode with two associated jets in the final state.

%Inclusive Volume
\begin{table}[!ht]
        \centering
        \begin{tabular}{ c |  c |  c}
        \hline\hline
        Category & Inclusive Requirement & Cut Value   \\
        \hline\hline
        Jets (anti-k${}_t$ R=0.4)       & $pT_{j}$				&   $\ge  30 GeV$  \\
        			        & $N_{\textrm{jets}}$           &   $\ge 2$      \\
        \hline
        \hline
        \end{tabular}
        \caption{Definition of the Inclusive Phase Space for VBF $H$ production.}
        \label{table:incl_volume}
        \end{table}

