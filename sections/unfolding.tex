The unfolding process corrects for detector effect such as inefficiencies and limited resolution in the distribution of kinematic variables. The corrected cross section measurements refer to the fiducial kinematic region, which corresponds to the same phase space of the detector level distributions, both for the jets and the boson decay products. 

The differential cross sections are determined for several kinematic variables using an iterative Bayesian unfolding. The number of iterations is optimized to find a balance between statistical uncertainties and bias in the measurement towards the simulated prediction. The unfolding procedure corrects for migrations between bins in the distributions during the reconstruction of events, and applies fiducial as well as reconstruction efficiency corrections. The fiducial corrections take into account events which are reconstructed in the signal region, but originate from outside the fiducial region; the reconstruction efficiency corrects for events inside the fiducial region which, are not reconstructed in the signal region due to detector inefficiencies. Tests with MC simulation demonstrate that this method is successful in retrieving the particle-level distribution in the fiducial region from the reconstructed distribution at detector level in the signal region.

\subsubsection{Iterative Bayesian unfolding}
The most straightforward technique for the data unfolding is the bin-by-bin correction. The ratio of the MC particle level (true) distribution to the MC reconstructed one is determined, and this ratio is used bin-by-bin to correct the measured observable. This approach, however, does not account for migration between bins, therefore, it is only valid if these migrations are small.
To account for bin to bin migrations we used the bayesian unfolding method. We want to determine the probability that a bin $i$ in the detector level distribution would lead to bin $j$ in the true distribution. According to Bayes theorem, the probability of an event ending up in bin $i$ from the truth-level distribution, while being in bin $j$ in the detector-level distribution, is given by:
\begin{equation}
P(T_{i}|R_{j})=\frac{P(R_{j}|T_{i}) \cdot P_{0}(T_{i}) }{ \sum_{k} P(R_{j}|T_{k}) \cdot P_{0}(T_{k})}
\label{eq:bayes}
\end{equation} 
The total bin content,n , of truth bin i is then given by:
\begin{equation}
n(T_{i})=\frac{1}{\epsilon_{i}}\sum_{j}n(R_{j})\cdot P(T_{i}|R_{j})
\end{equation}

Where $n(R_{J})$ is the total content of bin $j$ in the detector-level distribution and $\epsilon$ is the efficiency of the truth bin $i$. Efficiency is the ratio of events that pass both truth-level and detector-level selections divided by number of events that only pass the truth selection.  

For the algorithm we produce the response matrix $M_{ij}$ from equation~\eqref{eq:bayes}:
\begin{equation}
M_{ij}=\frac{P(R_{j}|T_{i}) \cdot P_{0}(T_{i}) }{ \sum_{k} P(R_{j}|T_{k}) \cdot P_{0}(T_{k}) \cdot\sum_{l}^{n({R})}P(R_{l}|T_{i})}
\end{equation}
where the last term $\sum_{l}^{n({R})}P(R_{l}|T_{i})$ is the efficiency factor discussed earlier. The truth distribution is then simply given by:
\begin{equation}
n(T_{i})=\sum_{j} M_{ij} \cdot n(R_{j})
\end{equation}
The migration matrix is trained with MC simulation with the same final cuts in the detector-level spectrum and truth-level spectrum. The distribution from the MC is taken as the a priori guess in the Bayesian method \cite{dagostini}. 


%%%%%%%%%%%%%%
\subsubsection{Validation tests}

The first test of the unfolding technique is to use the same MC pseudo-data for training and testing. This should result in a plot that perfectly fits the truth distribution from the detector-level spectrum up to statistical  uncertainties.  
Figure \ref{fig:unfolding_valid} shows the results of the validation test for several observables.  
\begin{figure*}[p]
\begin{multicols}{3}
    \includegraphics[width=\linewidth]{figures/observables/higgs_pt/higgs_pt_validation.pdf}\par 
    \includegraphics[width=\linewidth]{figures/observables/MTll/MTll_validation.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/lep_pt_0/lep_pt_0_validation.pdf}\par

    \end{multicols}
\begin{multicols}{3}
    \includegraphics[width=\linewidth]{figures/observables/DYll/DYll_validation.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/DYjj/DYjj_validation.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/jet_pt_0/jet_pt_0_validation.pdf}\par
	\end{multicols}
\begin{multicols}{3}
    \includegraphics[width=\linewidth]{figures/observables/Mll/Mll_validation.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/Mjj/Mjj_validation.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/Ptll/Ptll_validation.pdf}\par

	\end{multicols}
\caption{\label{fig:unfolding_valid} Validation plots for unfolding. The detector-level spectrum is in green and the truth spectrum in blue. The unfolded spectrum is shown with black markers that overlap with the truth spectrum.}
\end{figure*}

%%%%%%%%%%%

\subsubsection{Response Matrices}
Response matrix is a plot of truth events vs. reconstruction events for  a distribution. Response matrix shows how  much events migrate between bins due to detector effects. Figure \ref{fig:response_matrices} shows the response matrices for kinematic distributions. The X-axis is the reconstructed distribution and the Y-axis is the truth distribution.
\begin{figure*}[!htbp]
\begin{multicols}{3}
    \includegraphics[width=\linewidth]{figures/observables/higgs_pt/higgs_pt_migrations.pdf}\par 
    \includegraphics[width=\linewidth]{figures/observables//MTll/MTll_migrations.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/Mll/Mll_migrations.pdf}\par

    \end{multicols}
\begin{multicols}{3}
    \includegraphics[width=\linewidth]{figures/observables/DYll/DYll_migrations.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/Ptll/Ptll_migrations.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/Mjj/Mjj_migrations.pdf}\par

	\end{multicols}
\begin{multicols}{3}
    \includegraphics[width=\linewidth]{figures/observables/DYjj/DYjj_migrations.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/lep_pt_0/lep_pt_0_migrations.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/lep_pt_1/lep_pt_1_migrations.pdf}\par
	\end{multicols}
    \caption{\label{fig:response_matrices}Response matrices for kinematic distributions. The bin values corrspond to normalized bayesian probablilites}
\end{figure*}


\subsubsection{Statistical Uncertainty}
The statistical uncertainty on the unfolded distribution is propogated from the reconstruction level through randomly reweighing each bin in the reco distribution and using the same migration matrix to solve for the reweighted truth distribution. The random variations experiments are repeated one thousand times. Each variation taken randomly from a gaussian distribution with a center of the original content of each bin and with a standard deviation equal to the statistical uncertainty of each bin. For each of the one thousand toys the difference in the weighted truth content and the unfolded distribution is one event in the histogram.


\begin{figure*}[!htbp]
\begin{multicols}{3}
    \includegraphics[width=\linewidth]{figures/observables/higgs_pt/higgs_pt_stat_uncertainty.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/MTll/MTll_stat_uncertainty.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/DYll/DYll_stat_uncertainty.pdf}\par
	\end{multicols}
\begin{multicols}{3}
    \includegraphics[width=\linewidth]{figures/observables/DYjj/DYjj_stat_uncertainty.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/lep_pt_0/lep_pt_0_stat_uncertainty.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/lep_pt_1/lep_pt_1_stat_uncertainty.pdf}\par
	\end{multicols}
\begin{multicols}{3}
    \includegraphics[width=\linewidth]{figures/observables/Mll/Mll_stat_uncertainty.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/Mjj/Mjj_stat_uncertainty.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/Ptll/Ptll_stat_uncertainty.pdf}\par
	\end{multicols}
    \caption{\label{fig:all_stats} Statistical uncertainty propagated through unfolding vs. bayesian Iteration in Higgs Pt}
\end{figure*}



The statistical uncertainty is the variance in the gaussian histogram of each bin. Figure \ref{fig:all_stats} shows the estimated uncertainty on each unfolded distribution propagated through three bayesian iterations.



\subsubsection{Unfolding Bias and Coverage}
The purpose of the closure test is to check the bias of the unfolded distribution. Any regularization method trained monte carlo will bias the unfolded distribution towards the monte carlo data, potentially hiding new physics in the real data.

To measure the bias, a new truth distribution is created by varying the bins in the original distribution by a random number drawn from a gaussian centered on the nominal truth content with a width equal to the uncertainty on the bin. The corresponding reconstruction-level distribution is estimated by folding the fluctuated truth distribution using the migration matrix trained from MC. The folded reco distribution is unfolded using  iterative bayesian method. The bias is the difference between the unfolded distribution and the fluctuated truth distribution. Figure \ref{fig:2dcoverage_plots} shows the relation between the unfolded  bin values on the y-axis and the fluctuated truth values on the x-axis for higgs pt distribution. 

\begin{figure*}[!htbp]
\begin{multicols}{3}
    \includegraphics[width=\linewidth]{figures/observables/Mjj/Mjj_coverage_bin2_iteration_3.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/Mjj/Mjj_coverage_bin3_iteration_3.pdf}\par
     \includegraphics[width=\linewidth]{figures/observables/Mjj/Mjj_coverage_bin4_iteration_3.pdf}\par
	\end{multicols}
\begin{multicols}{3}
    \includegraphics[width=\linewidth]{figures/observables/Mll/Mll_coverage_bin2_iteration_3.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/Mll/Mll_coverage_bin3_iteration_3.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/Mll/Mll_coverage_bin4_iteration_3.pdf}\par
	\end{multicols}
\begin{multicols}{3}
    \includegraphics[width=\linewidth]{figures/observables/higgs_pt/higgs_pt_coverage_bin2_iteration_3.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/higgs_pt/higgs_pt_coverage_bin3_iteration_3.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/higgs_pt/higgs_pt_coverage_bin4_iteration_3.pdf}\par
	\end{multicols}
    \caption{\label{fig:2dcoverage_plots}Truth Cross Section vs. Unfolded Cross Section for three  Bayesian iterations }
\end{figure*}

The method of evaluating systematic uncertainties can be used to find the optimal normalization parameter for unfolding, which is the number of iterations in the Baysian algorithm in this case. Figure \ref{fig:Higgs_pt_prof} shows the profiles for the 2D histograms with several iterations. The larger slope indicates a larger bias towards the monte carlo. More iterations reduce the maximum bias for large fluctuations.



\begin{figure*}[!htbp]
\centering
\begin{multicols}{3}
    \includegraphics[width=\linewidth]{figures/observables/higgs_pt/higgs_pt_profiles_bin1.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/higgs_pt/higgs_pt_profiles_bin2.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/higgs_pt/higgs_pt_profiles_bin3.pdf}\par

	\end{multicols}
\begin{multicols}{3}
   \includegraphics[width=\linewidth]{figures/observables/Mll/Mll_profiles_bin1.pdf}\par
   \includegraphics[width=\linewidth]{figures/observables/Mll/Mll_profiles_bin2.pdf}\par
   \includegraphics[width=\linewidth]{figures/observables/Mll/Mll_profiles_bin3.pdf}\par
	\end{multicols}
\begin{multicols}{3}
    \includegraphics[width=\linewidth]{figures/observables/Mjj/Mjj_profiles_bin1.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/Mjj/Mjj_profiles_bin2.pdf}\par
    \includegraphics[width=\linewidth]{figures/observables/Mjj/Mjj_profiles_bin3.pdf}\par

	\end{multicols}
    \caption{\label{fig:Higgs_pt_prof}Profile of Systematic Bias vs Variation in Truth Content }
\end{figure*}

\subsubsection{Obsevables Binning}
Observable binnings are chosen so that the purity and efficiency are above 0.7 and theres at least 10 expected VBF events for each bin. The are shown in table \ref{table:binning}
\\
\\
\begin{table}[!ht]
\centering
\begin{tabular}{ |p{4cm}||p{8cm}|  }
 \hline
 Observable & Bin Edges\\
 \hline
 $MT^{l,l,MET}$ &15,40,65,80,95,110,115,120,180\\
 $Mll$&  0,20,25,30,35,40,45, 50, 60, 70,200\\
 $Mjj$ &200,450,700,950,1200,1500,2200,3000,5000\\
 $Cos(\theta^{*})$ &0,0.1,0.2,0.3,0.4,0.6,1.0\\
 $DYll$&   0,0.15,0.3,0.45,0.6,0.75,0.9,1.05,1.2,1.5,4\\
 $DYjj$& 0,2.5,3.25,3.62,4,4.35,4.75,5,5.5,6.25,7,8.5\\
 $P_{t}$ Total& 150,200,250,350,500,600,900\\
 $DPhill$& 0,0.1,0.2,0.3,0.4,0.6,0.8,1.0,1.4,1.8,2.1,2.6,3.2\\
 $P_{t} ll$&0,20,40,50,60,70,80,90,100,120,140,500\\
 Higgs $P_{t}$&0,45,80,120,160,200,260,350,1000\\
 Leading Lepton $P_{t}$&20,25,35,40,50,65,80,90,200\\
 Subleading Lepton $P_{t}$&20,25,35,40,50,65,80,90,200\\
 Leading Jet $P_{t}$&30,60,90,120,190,260,350,500\\
 Subleading Jet $P_{t}$& 30,60,90,120,190,260,350\\
 \hline
 \hline
\end{tabular}
 \caption{Observable Binnings}
 \label{table:binning}

\end{table}
