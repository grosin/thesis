\documentclass[11pt]{article}
%to do, add in all the actual valildation plots and effeciency tables
\usepackage{amsmath}
\usepackage{graphicx}
\begin{document}

Quality control for modules is used to ensure high and constant electrical and mechanical quality of the modules and determine if the modules will survive the expected 10 year lifetime in the ATLAS detector. The QC procedure will consist of electrical, mechanical and functional tests that look for sensor deformation, glue shape changes, electrical characteristic of sensors, i.e: I-V
and C-V curves and test of the front-end electronic.

The plan for module quality control during production will involve electrical tests, thermal cycling, and HV stability tests. Modules will be cycled from $40^{o}$ C to $-35^{o}$ C ten times to simulate wear and tear over the lifetime of the  detector. Following the ten cycles, the high voltage will be left on for two hours to measure the stability of the current through the sensor. The thermal cycle will begin strobe delay, three point gain and trim range to determine trim DAC values for warm and cold phases of the cycle. 

To conduct QC tests, BNL and Harvard University developed a cold box shown in figure \ref{fig:cold_box} to test multiple modules at once and thermal cycle them. The cold box contains four cooling chucks similar to the cooling chuck used in the  single module box.

\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.08]{figures/four_module_box.jpg}
     \caption{\label{fig:cold_box}BNL/Harvard four module cold box}
\end{figure*}
The electrical connections to the box are outlines in figure \ref{fig:qc_box_schematic}. The four modules share a common high voltage connections, a decision driven by limited Keithley HV supplies. Module high voltage can be controlled by the HV multiplexer on the power board. The tests described below used the raspberry pi I$^{2}$C buses to control the AMAC and read out the sensors. The newerv version of the AMAC, uses a costume I$^{2}$C like protocol that will be read out by the FPGA. The lid of the box had an HV interlock to kill high voltage if the box is opened. The temperature for the first tests was read out by thermocouples using a TC-08 that were in thermal contact with the sensor. During production the plan is to use thermistors on the hybrids to measure the temperature, readd out by the AMAC.  Finally, the box was grounded to be a Faraday cage (but is made up of
steel which has poor conductivity).

\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.8]{figures/qc_box_electrical_schematic.pdf}
     \caption{\label{fig:qc_box_schematic}Schematic of electrical connections for the four module box}
\end{figure*}

The cooling system will be controlled by a Raspberry Pi, a small single board computer, acting as a server with the user interacting remotely. The Raspberry Pi will be connected to an analog to digital converter to read out environmental sensors from the Raspberry Pi's general input output (GPIO) pins. A sensor will be needed to measure the humidity inside the box, the ambient temperature, the nitrogen flow rate and the vacuum pressure in the chucks. The Pi will use USB to serial connections to communicate with the slow controls and the chiller. I developed the software framework that will monitor the environmental sensors as well as control the chiller and the power supplies and run data acquisition tests in synchronization with the temperature sensors on the strip module. Figure \ref{fig:outline} shows an outline for the the software framework described above. One of the biggest challenges we will face is to set up the system to run the tests sufficiently quickly to test several modules per day. There are many choke points that can slow down the testing cycle such as the time for the chiller to cool down and how fast the high voltage can be ramped to determine the IV curve.


\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.4]{figures/dcsdaq_scheme.png}
     \caption{\label{fig:outline}Outline of Software Framework for QC Tests}
\end{figure*}


The setup for the data acquisition system is shown in figure \ref{qc_box_daq_setup.pdf}. The enclosure was 3D printed at BNL. On the top left is a Nexys FPGA connected to the central computer through an ethernet connection. The DAQ commands are run through the FPGA and data is histogramed then sent to the computer through a UDP protocol. On the bottom is the raspberry pi, with a I$^{2}$C multiplexer to run the DCS. Finally on the left are two Low-voltage differential signaling (LVDS) to  Scalable Low-Voltage Signaling (SLVS) converter, each one reading out two modules (so four modules in total). 


\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.65]{figures/qc_box_daq_setup.pdf}
     \caption{\label{fig:daq_setup}Setup for cold box DAQ}
\end{figure*}

The first test of the thermal cycle with two modules is shown in figures \ref{fig:first_qc_test} and \ref{fig:first_qc_test_daq_tests}. Figure \ref{fig:first_qc_test} shows the temperatures over four cycles, with a time range done in the first cycle in the hot and cold portion, and the trim range configurations used in the following cycles. Figures \ref{fig:first_qc_test_daq_tests} shows the estimated time for DAQ tests in each portion of the cycle.  Strobe delay takes ~10 minutes, three point gain ~20 minutes, trimRange ~35 minutes and the low statistics 3 point gain around 5 minutes. The goal is to run 12 cycles over 10 hours. Figure \ref{fig:chiller_rate} shows the temperature rate for cooldown and warmup. In both cases its around 20 minutes, so to keep the whole test under 12 hours, the tests in total should take about 10 minutes, which necessitates low statistics tests. 

\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.70]{figures/first_qc_test.pdf}
     \caption{\label{fig:first_qc_test}Temperature sensor outputs from the first DAQ test}
\end{figure*}

\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.65]{figures/first_qc_test_daq_test.pdf}
        \includegraphics[scale=0.70]{figures/first_qc_test_daq_test_2.pdf}
     \caption{\label{fig:first_qc_test_daq_tests}Top plot is the first cycle where the trim range was run, the second plot is the second thermal cycle where only strobe delay and three point gain were run. The times each test took is on the x axis. SD is strobe delay,  3PG is thee point gain, 32T-3PG is three point gain with low statistics. }
\end{figure*}

\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.5]{figures/qc_box_temperature_rate.pdf}
     \caption{\label{fig:chiller_rate} Cooldown and warm up rate for the four module box with the SPS chiller. Warmup is slightly faster as expected but its close to 20 minutes each. }
\end{figure*}


figure \ref{fig:thermal_cycle} is the sensor recordings from the first QC run with four module  in the BNL box with ten thermal cycles and an  HV stability test. The top left plot is the air temperature, which gets as cold as 0$^{o}$ C. The two plots on the bottom left are the thermocouple temperature in thermal contact with the cooling chuck, used to automate the thermal  cycle. The top right plot is the box relative humidity, which consistently stayed below 5\%. The goal is less than 10\% humidity to avoid frosting. The middle left plot is the high voltage  stability test. Finally the bottom right plot is vacuum pressure used to hold down th chuck. The vacuume pressure goes down by approximately 2 psi at cold temperature because the sensor shape deforms and lifts off the vacuum plates slightly. Figure \ref{fig:vacuum_pressure} shows the vacuum pressure vs. temperature. There's a hysteresis effect where the cooldown shape deformation is different than the warm up shape  deformation. 

\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.28]{figures/thermal_cycle.png}
     \caption{\label{fig:thermal_cycle}Sensor recordings from thermal cycle}
\end{figure*}


\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.4]{figures/daq_box_vacuum_pressure.pdf}
     \caption{\label{fig:vacuum_pressure}Vacuum pressure vs. temperature for vacuum chucks in the four module box. The path on the left is the plot as the box cools down, the path on  the right is  the plot as the box warms up}
\end{figure*}

Figure \ref{fig:four_module_box_itsdaq} shows the results of the DAQ tests for the first thermal cycle for  a single module.


\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.8]{figures/qc_box_daq_tests_results.pdf}
     \caption{\label{fig:four_module_box_itsdaq}DAQ test result for four module box with all four modules and the full thermal cycle at the warm and cool part of the cycle. The top plot is the Vt50 of each channel, the two middle plots is the estimate of the gain, the bottom plot is input noise}
\end{figure*}

Figures \ref{fig:thermal_cycle_input_noise} and \ref{fig:thermal_cycle_output_noise} shows the input and output noise distribution of  all the modules in the DAQ tested conducted in the warm part of the cycle vs. the cold part of the cycle. The distribution of noise looks healthy in both cases, with lower noise from tests conducted when the modules are cold, as expected. Finally figure \ref{fig:average_input_noise} shows the average noise of all the modules over the course of the 10 thermal cycles.


\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.7]{figures/qc_box_output_input_noise_comparison.pdf}
     \caption{\label{fig:thermal_cycle_input_noise}Distribution of the input noise for all four modules in the warm and cold part of the thermal cycle. Room temperature expectations are 600-700 ENC, so the noise in the warm up cycle }
\end{figure*}


\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.7]{figures/qc_box_output_noise_comparison.pdf}
     \caption{\label{fig:thermal_cycle_output_noise}Distribution of the output noise for all four modules in the warm and cold part of the thermal cycle }
\end{figure*}

\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.5]{figures/average_input_noise_thermal_cycle.pdf}
     \caption{\label{fig:average_input_noise}Average Input noise for all modules for each of the ten thermal cycles }
\end{figure*}
\end{document}
