\section{Fiducial and inclusive phase space}
The experimental measurement of VBF h $\rightarrow WW \rightarrow \ell \nu \ell \nu$ production in the the e$\mu$ decay channel is reported as a fiducial cross section defined as follows:

\begin{equation}
  \sigma^{\mathrm{fid}}_{\textrm{VBF}  h \rightarrow ww \rightarrow l \nu l \nu}
  = \frac{N_{\textrm{obs}} - N_{\textrm{bkg}}}{C \times {\cal{L}} },
\label{xSectFid}
\end{equation}

where ${\cal{L}}$ is the integrated luminosity, $N_{\textrm{obs}}$ is the observed number of events, $N_{\textrm{bkg}}$ is the estimated number of background events and $C$ is a factor that accounts for detector inefficiencies and resolution effects.
The latter is defined as the ratio of the number of reconstructed signal events after the final selection with electrons or muons in the final state (including electrons or muons from $\tau$-decays) to the number of signal events generated in the fiducial region where only direct decays of $W$ bosons to electrons and muons are considered.


The VBF $W \rightarrow WW \rightarrow \ell \nu \ell \nu$ cross section is evaluated in the fiducial phase space of the $e-\mu$ decay channel, as defined in Table \ref{tab:fid_volume} for the standard model Higgs of 125 GeV. The definition of the fiducial phase space is chosen to be as close as possible to the kinematic phase space at detector level in order to minimize theoretical extrapolation and model dependence.

Electrons and muons are required at particle level to stem from one of the W bosons produced in the hard scatter and are defined at the "dressed level", i.e. the momenta of photons emitted in a cone R = 0.1 around the lepton direction are added to the charged  lepton momentum after QED final-state radiation. Final-state particles, defined as those with a proper lifetime $\tau$ corresponding to $c\tau \ge 10$ mm, are clustered into jets (referred to as particle-level jets) using the same algorithm as for detector-level
jets, i.e. the anti-k${}_t$ algorithm with radius parameter R = 0.4.
The selected charged leptons and neutrinos from $W$ boson decays are not included in the jet clustering.
The fiducial phase space at particle level does not make any requirement on jets containing $b$-quarks.
The missing transverse momentum is defined at particle level as the transverse component of the vectorial sum of non-interacting particles. Its magnitude is denoted in Table \ref{tab:fid_volume} as MET.
If a jet is within $\Delta$R=0.4 from the selected electron or muon, the event is not considered \cite{Banfi1}.


%Fiducial Volume
\begin{table}[!ht]
        \centering
        \begin{tabular}{ c |  c |  c}
        \hline\hline
        Category & Fiducial Requirement & Cut Value   \\
        \hline\hline
        		   	   	& $|Eta|$                      &   $<$ 2.5  \\		
					& $P_{T}$lead      			&   $> 22 GeV$   \\
           Leptons ($e+\mu$, $\mu+e$)  	& $P_{T}$sublead   			&   $> 15 GeV$  \\
	   	   			
	   	   			&  OS and OF pair     		&  Yes       \\
					& $\Delta$R($\ell,\ell$)       &   $> 0.1$ \\
						& $\Delta \phi _{\ell \ell}$       &   $< 1.4$ \\
           	   			& $mll$                  	&   $> 10 GeV$  \\
        \hline
        Neutrinos  			& $met$                        &   $> 20 GeV$   \\
	\hline
		   			& pTj 				&   $> 30 GeV$  \\
	                & $|\eta_{j}|$ 		        &   $< 4.5$  \\
   Jets (anti-k${}_t$ R=0.4)& $N_{\textrm{jets}}$           &   $\ge 2$      \\	
                    & $N_{\textrm{b-jet}}$           &   $< 1$      \\
                    & $M_{jj}$                      & $> 450 GeV$ \\
                    & $DY_{jj}$                      & $> 2.1$  \\
	\hline
	 lepton-jet separation		& $\Delta$R($\ell$,jet) 		&   $> 0.4$ \\
	\hline
	VBF	   			& OLV                           &   Yes   \\
          	   			& CJV                           &   Yes   \\
        \hline
        \hline
        \end{tabular}
        \caption{Definition of Fiducial Phase Space for VBF $h \rightarrow ww \rightarrow \ell \nu \ell \nu$  in the $e-\mu$ decay channel.}
        \label{tab:fid_volume}
        \end{table}



The fiducial cross section is extrapolated in a broader phase space, referred to as the "inclusive" phase space in the following. An acceptance correction factor $A$ is used to extrapolate the fiducial cross section to an inclusive cross section, according to the following formula:

\begin{equation}
  \sigma^{\mathrm{incl}}_{VBF}
  = \frac{\sigma^{\mathrm{fid}}_{\textrm{VBF}}}{A},
\label{xSectIncl}
\end{equation}

Theoretical calculations are used to estimate the acceptance correction $A$ from the fiducial to the inclusive phase space, and the extrapolated cross section is referred to in the following as inclusive cross section. The theoretical cross section is calculated at NNLO in QCD and NLO at EW, and the prediction given is

\begin{equation}
 \sigma^{theory}_{VBF,H\rightarrow WW\rightarrow \ell \nu \ell \nu} = 85.8 \pm 2.2 \text{ fb}
 \end{equation}
 The definition of the inclusive phase space is summarized in Table \ref{tab:incl_volume} \cite{Florian1}. The inclusive cross section is extracted for Higgs production in the vector boson fusion mode with two associated jets in the final state.

%Inclusive Volume
\begin{table}[!ht]
        \centering
        \begin{tabular}{ c |  c |  c}
        \hline\hline
        Category & Inclusive Requirement & Cut Value   \\
        \hline\hline
        Jets (anti-k${}_t$ R=0.4)       & $pT_{j}$				&   $\ge  30 GeV$  \\
        			        & $N_{\textrm{jets}}$           &   $\ge 2$      \\
        \hline
        \hline
        \end{tabular}
        \caption{Definition of the Inclusive Phase Space for VBF $H$ production.}
        \label{tab:incl_volume}
        \end{table}


The differential cross sections are determined for kinematic variables unfolded to particle-level in
the same fiducial phase space as in Table \ref{tab:fid_volume}. The unfolding procedure corrects for migrations between bins in
the distributions during the reconstruction of events, and applies fiducial as well as reconstruction efficiency
corrections. The fiducial corrections take into account events which are reconstructed in the signal region, the reconstruction efficiency corrects for events inside the fiducial region which are not reconstructed in the signal region due to detector inefficiencies. Tests with
MC simulation demonstrate that the method is successful in retrieving the particle-level distribution in
the fiducial region from the reconstructed distribution at detector level in the signal region.

\section{ Detector And Acceptance Corrections}
The C factor described in the previous section, is defined as $N_{detector}/N_{truth}$, the estimated yield in the reconstruction level Monte Carlo over the estimated yield in the truth level Monte Carlo. The C-factor value is reported in table \ref{tab:fiducial_c_acceptance}

The acceptance factor is defined as $A =N^{truth}_{fid}/N^{truth}_{inc}$ = $\sigma^{truth}_{fid}/\sigma^{truth}_{inc}$,where the inclusive cross section is taken  from the Higgs Cross Section Working Group for Yellow Book Report 4 \cite{Heinemeyer1}, and the fiducial cross section is calculated at truth level at NLO in QCD and EW, including the theoretical uncertainties described in the following chapters. The results of the fiducial cross section estimates is 
\begin{equation}
 \sigma^{truth}_{fid} = 2.07 \pm 0.09 \text{ fb}
 \end{equation}
 And the acceptance factors for three inclusive phase spaces are in table \ref{tab:fiducial_c_acceptance} \cite{Tanabashi1}.
 
\begin{table*}[!htbp]
        \centering
    \includegraphics[scale=0.45]{figures/supporting_note/table4.pdf}
     \caption{\label{tab:fiducial_c_acceptance} C-factor and Acceptance factor.
}
\end{table*}
