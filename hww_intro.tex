\documentclass[11pt]{article}
%to do, add in all the actual valildation plots and effeciency tables
\usepackage{amsmath}
\usepackage{graphicx}
\begin{document}
\section{Introduction}
The analysis presents a measurement of the decay of standard model Higgs boson to two W bosons with a final state of two opposite sign leptons and two neutrinos. A standard model Higgs boson candidate is identified by starting from data with two isolated charged opposite-sign and different flavor leptons ($e\mu$, $\mu e$), and missing transverse energy $MET$ due to the presence of neutrinos in the final state. Kinematic properties of the dilepton system are also used to identify the SM Higgs boson. The SM Higgs boson with spin zero decays in two $W$ bosons with spins anti-aligned. This spin configuration propagates to the $W$'s decay products through the conservation of angular momentum and the structure of the weak interactions. Due to the spin correlations, the angle between the charged leptons is relatively small. The VBF production mode is identified by requiring at least two associated highly energetic jets ($tagging$ jets) originating from the initial state quarks. It is distinguished from other production modes, most notably the gluon-gluon fusion ($ggF$) mode, by kinematic requirements. For example a veto on the $W$ leptons outside the rapidity region defined by the two taggging jets (outside lepton veto or OLV), and a veto on jets produced inside the rapidity region defined by the two taggging jets (central jet veto or CJV).



The main background processes that contribute to the $h \rightarrow ww \rightarrow \l \nu \l\nu$ analysis are: top, $WW$, $WZ$, $W\gamma*$, $ZZ$, $W\gamma$, $W$ + jets and $Z \rightarrow ee,mm,tt$. Top and $WW$ have two $W$ bosons in the final state, similar to the signal. The presence of jets, especially those tagged as $b$-jets, is used to discriminate against top. Spin correlation kinematics are used to reject $WW$. Two or more real leptons are produced by the $Ztt$ and Non-$WW$ background ($WZ$, $W\gamma*$, $ZZ$). These have a smaller cross section but more signal-like kinematics because of the softer subleading lepton, particularly in the case of $W\gamma*$. The $W$ + jets production has a high cross section and pass the signal region selection because a jet is mis-reconstructed as an isolated lepton.

\subsection{Motivations}

There are two predominant Higgs production channels in the LHC: gluon-gluon Fusion (ggF) and Vector Boson Fusion (VBF), shown in Figure \ref{fig:higgs_production}. The ggF process is the dominant Higgs
production mode and consists of two gluons fusing into a quark loop that then produces a Higgs. The Higgs-quark coupling drives this production mode. VBF is the secondary production channel and forms a Higgs when two quarks each radiate a massive weak boson. These bosons fuse together to create a Higgs, thus this decay is mediated by HWW coupling. The two interacting
quarks hadronize to form two jets which exhibit a distinct energetic signature in the forward and backward regions. Physicists at ATLAS and CMS have measured the strength of the Higgs couplings to other particles and thus far all are compatible with the SM Higgs boson. However,the experimental uncertainty on the VBF production cross-section is about $30\%$ in Run-1 analyses. In Run-2 the sensitivity of the measurement to anomalies will be improved due to the larger data set, more Monte Carlo statistics, and more sophisticated analysis techniques including multivariate analysis tools such as deep neural networks and boosted decision trees. 

\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.1]{figures/higgs_production.png}
     \caption{\label{fig:higgs_production} Feynamn diagrams of Higgs production to WW decay}
\end{figure*}

This analysis looks at the The opposite flavor channel to avoids a large background from $Z\rightarrow ee$ and $Z\rightarrow \mu \mu$ processes. These backgrounds require tight cuts on the invariant dilepton mass (Mll) which limits total possible signal events in the signal region \cite{HWW}. 

\section{Analysis Stategy Overview}
The full run-2 data set is analysed, corresponding to the 2015-2018 data sets at $\sqrt{s} = 13$ TeV proton-proton centre-of-mass energy, and to an integrated luminosity of 139 fb$^{-1}$ . A pre-selection is applied on recorded
events to select Higgs boson production processes in which the Higgs boson decays into a W boson pair
that in turn decays into the fully leptonic channel with two opposite flavour leptons, i.e. an electron and
a muon, and neutrinos in the final state.

The main background processesare $t\bar{t}$, single-top production, dibosons, Z+jets, also referred to as Drell-Yann (DY), fakes, where jets are misreconstructed as leptons such W+jets and QCD
multijet production, as well as other Higgs production modes, such as ggF, VH, ttH. Major backgrounds
( $t\bar{t}$,, single-top production, dibosons, Z+jets, fakes and ggF Higgs boson production),
are estimated in a data driven way, while all other backgrounds (other Higgs production modes) are estimated by MC.


After preselection several additional selections are applied to define a VBF signal region (SR), and several control regions (CRs) that are enriched
in the major background processes: DY-CR and ggF-CR. The top backgrounds are estimated from a fit within the signal region. In the SR kinematic requirements such as
OLV and CJV are applied to select VBF candidate events,
the DY-CR exploits kinematic variables sensitive to the different spin configuration of DY and Higgs
decay events, and finally a single ggF enriched control region. In addition, the contamination from the
fake background in SR and CRs is estimated in data using a dedicated sample enriched in fake leptons. All the signal, control and validation regions are described in tables \ref{tab:overview_of_phasespace} and \ref{tab:overview_of_phasespace2}, where validation regions are used to determine if the Monte Carlo modelling matchs the data. 

To extract the fiducial and differential cross sections, a simultaneous
fit of SR and CRs to data is carried out to estimate the amount of $t\bar{t}$ , DY and ggF background events based
on data as well as to extract the signal yield. In the fit the signal strengths of the VBF signal, the $t\bar{t}$ , VV
(V = W or Z), DY and ggF processes are left floating, and the signal strength of v is considered in combination
with VV, since it was determined that there's not enough discrimination power between them. The signal yield is extracted by multiplying the signal strength by the theoretical cross section
and is then corrected for experimental efficiency (C-factor) and integrated luminosity (L) to finally extract
the signal cross section that is compared to theoretical predictions.

Boosted Decision Trees (BDTs) are used to discriminate between the various processes in the statistical fit. For the signal region, a two-dimensional
BDT is trained to discriminate the signal from $t\bar{t}$ and diboson (vbf bdt) and discriminate  $t\bar{t}$ and diboson from all other processes (Top+wW bdt).The signal region is then split into two parts, signal region 1 with vbf bdt $> 0.5$ and signal region 2 with vbf bdt $<0.5$. In signal region 1 the fit is done over a projection onto the vbf bdt, in the signal region 2 the fit is done over a projection onto the Top+WW bdt.

For the ggF background
estimation one CR is used and a dedicated BDT ( ggF BDT) is trained to increase
the separation between ggF and all other processes, including VBF. The ggF BDT output distribution is
used in the simultaneous fit, i.e. no cut is applied on this the ggF BDT output. Finally the fit in the Drell-Yann control region is done over the $m_{T}$, which was determined to have sufficient discriminatory power compared to a dedicated Zjets BDT.

For each differential cross
section measurement a different BDT is trained by removing from the list of input variables the measured
kinematic variable and those that are highly correlated to it, called N-1 bdts in following sections. Experimental and theoretical uncertainties are
propagated into the simultaneous fit as nuisance parameters and are profiled by the fit.

\begin{table*}[!htbp]
	\centering
    \includegraphics[scale=0.35]{figures/supporting_note/table1.pdf}
     \caption{\label{tab:overview_of_phasespace} Kinematic cuts on the defined phase space regions in the analysis. Definitions of  $m_{T}$ and $m_{T}2$ are given the following chapters on observables. }
\end{table*}

\begin{table*}[!htbp]
	\centering
    \includegraphics[scale=0.35]{figures/supporting_note/table2.pdf}
     \caption{\label{tab:overview_of_phasespace2} Kinematic cuts on the defined phase space regions in the analysis. The requirements that are modified with respect to the SR are highlighted in bold.  }
\end{table*}


Differential cross sections are measured as a function of kinematic observables unfolded to particle
level in the fiducial phase defined in the next chapter. Here follows the list of 13 distributions that are measured and unfolded to
particle level:

\begin{itemize}
\item Higgs boson transverse momentum ($P^{T}_{H}$)
\item Leading and sub-leading jet transverse momenta ($P^{T}_{j0},P^{T}_{j1}$)
\item Leading and sub-leading lepton transverse momenta ($P^{T}_{l0},P^{T}_{l1}$)
\item di-lepton transverse momentum ($P^{T}_{ll}$)
\item di-lepton rapidity ($|\Delta y_{ll}|$)
\item di-jet rapidity (between the two leading jets) ($|\Delta y_{jj}|$)
\item di-lepton azimuthal angle ($|\Delta \phi_{ll}|$)
\item di-jet azimuthal angle ($\Delta \phi_{jj}$)
\item di-lepton invariant mass ($M_{ll}$)
\item di-jet invariant mass ($M_{jj}$)
\item cos($\theta^{*}$)=tanh($(\eta^{+}-\eta^{+})/2$) defined by the two leading charged lepton directions relative to the beam direction in a frame where the two leptons are back-to-back in the $r-\theta$ plane.
\end{itemize}

Finally the detector level cross section estimate is unfolded to particle level, meaning effects from detector ineffeciecies and migrations between detector and fiducial level bins is accounted for. 
The unfolding is implemented in the same step as the simultaneous fit, by embedding migration matrices into the likelihood definition. The binning of the unfolded distributions are
optimised based on the data statistics, efficiency, purity, systematic contributions, model bias as well as to
match the bin boundaries of other Higgs and SM electroweak differential measurements.

 The particle-level
differential cross sections as well as the fiducial cross section are compared to theoretical predictions
calculated at different orders of perturbative QCD and electroweak corrections.
The particle-level differential cross section measurements are interpreted in an Effective Field Theory
(EFT) formalism to set limits on anomalous couplings that may effect the interaction vertices involved in
the production mechanism and decay of the vector-boson-fusion $H \rightarrow W + W$ process.

\end{document}

