\contentsline {FrontMatter}{\MakeTextUppercase {\uppercase {Abstract}}}{ii}{chapter*.1}%
\contentsline {Chapter}{\numberline {1.}\MakeTextUppercase {Introduction}}{1}{chapter.1}%
\contentsline {Chapter}{\numberline {2.}\MakeTextUppercase {Theory}}{3}{chapter.2}%
\contentsline {subsection}{\numberline {2.0.1}standard model}{3}{subsection.2.0.1}%
\contentsline {subsubsection}{\numberline {2.0.1.1}Particles and their interactions}{3}{subsubsection.2.0.1.1}%
\contentsline {subsection}{\numberline {2.0.2}Quantum Electrodynamics}{6}{subsection.2.0.2}%
\contentsline {subsection}{\numberline {2.0.3}Quantum Chromodynamics}{9}{subsection.2.0.3}%
\contentsline {subsection}{\numberline {2.0.4}Electroweak}{10}{subsection.2.0.4}%
\contentsline {subsubsection}{\numberline {2.0.4.1}Sponteous Symmetry Breaking}{11}{subsubsection.2.0.4.1}%
\contentsline {subsection}{\numberline {2.0.5}LHC Physics}{15}{subsection.2.0.5}%
\contentsline {subsubsection}{\numberline {2.0.5.1}Higgs Physics}{18}{subsubsection.2.0.5.1}%
\contentsline {subsubsection}{\numberline {2.0.5.2}Whats Next?}{21}{subsubsection.2.0.5.2}%
\contentsline {Chapter}{\numberline {3.}\MakeTextUppercase {Atlas Detector}}{23}{chapter.3}%
\contentsline {section}{\numberline {3.1}The ATLAS dectector}{24}{section.3.1}%
\contentsline {Chapter}{\numberline {4.}\MakeTextUppercase {Inner Tracker}}{30}{chapter.4}%
\contentsline {Chapter}{\numberline {5.}\MakeTextUppercase {Data Acquisition}}{33}{chapter.5}%
\contentsline {subsection}{\numberline {5.0.1}stave test}{37}{subsection.5.0.1}%
\contentsline {Chapter}{\numberline {6.}\MakeTextUppercase {Module Quality Control}}{44}{chapter.6}%
\contentsline {Chapter}{\numberline {7.}\MakeTextUppercase {ABCStar Irradiation}}{54}{chapter.7}%
\contentsline {subsection}{\numberline {7.0.1}Physics Model}{56}{subsection.7.0.1}%
\contentsline {section}{\numberline {7.1}Results}{60}{section.7.1}%
\contentsline {Chapter}{\numberline {8.}\MakeTextUppercase {HWW VBF Cross Section}}{65}{chapter.8}%
\contentsline {subsection}{\numberline {8.0.1}Motivations}{66}{subsection.8.0.1}%
\contentsline {subsection}{\numberline {8.0.2}Analysis Stategy Overview}{67}{subsection.8.0.2}%
\contentsline {Chapter}{\numberline {9.}\MakeTextUppercase {Physics objects}}{72}{chapter.9}%
\contentsline {section}{\numberline {9.1}Tracking}{72}{section.9.1}%
\contentsline {section}{\numberline {9.2}Leptons}{73}{section.9.2}%
\contentsline {subsection}{\numberline {9.2.1}Electrons}{73}{subsection.9.2.1}%
\contentsline {subsection}{\numberline {9.2.2}preselection}{74}{subsection.9.2.2}%
\contentsline {subsection}{\numberline {9.2.3}Identification}{74}{subsection.9.2.3}%
\contentsline {section}{\numberline {9.3}Muons}{74}{section.9.3}%
\contentsline {subsection}{\numberline {9.3.1}Jets}{75}{subsection.9.3.1}%
\contentsline {subsection}{\numberline {9.3.2}Particle Flow}{75}{subsection.9.3.2}%
\contentsline {subsection}{\numberline {9.3.3}Jet Selection}{76}{subsection.9.3.3}%
\contentsline {subsection}{\numberline {9.3.4}b-tagging}{77}{subsection.9.3.4}%
\contentsline {subsection}{\numberline {9.3.5}Missing Transverse Energy}{77}{subsection.9.3.5}%
\contentsline {section}{\numberline {9.4}Overlap removal}{78}{section.9.4}%
\contentsline {Chapter}{\numberline {10.}\MakeTextUppercase {Event Selections}}{80}{chapter.10}%
\contentsline {section}{\numberline {10.1}Pre-selection}{82}{section.10.1}%
\contentsline {section}{\numberline {10.2}Signal Region Selection}{86}{section.10.2}%
\contentsline {section}{\numberline {10.3}Z+Jets Control Region}{90}{section.10.3}%
\contentsline {section}{\numberline {10.4}Top Validation Region}{93}{section.10.4}%
\contentsline {subsection}{\numberline {10.4.1}ggF Control and Validation region}{96}{subsection.10.4.1}%
\contentsline {subsection}{\numberline {10.4.2}W+jets Control Region}{107}{subsection.10.4.2}%
\contentsline {Chapter}{\numberline {11.}\MakeTextUppercase {Fiducial Region}}{108}{chapter.11}%
\contentsline {section}{\numberline {11.1}Fiducial and inclusive phase space}{108}{section.11.1}%
\contentsline {section}{\numberline {11.2} Detector And Accepetance Corrections}{111}{section.11.2}%
\contentsline {Chapter}{\numberline {12.}\MakeTextUppercase {Backgrounds}}{112}{chapter.12}%
\contentsline {section}{\numberline {12.1}Summary Of Backgrounds}{112}{section.12.1}%
\contentsline {section}{\numberline {12.2}Fake Lepton Backgrounds}{115}{section.12.2}%
\contentsline {section}{\numberline {12.3}Zjets Fake Factor Definition}{117}{section.12.3}%
\contentsline {subsection}{\numberline {12.3.1}Pileup Dependence}{121}{subsection.12.3.1}%
\contentsline {subsubsection}{\numberline {12.3.1.1}jet dependence}{125}{subsubsection.12.3.1.1}%
\contentsline {subsection}{\numberline {12.3.2}jet dependence}{128}{subsection.12.3.2}%
\contentsline {subsection}{\numberline {12.3.3}Fake Factor Corrections}{132}{subsection.12.3.3}%
\contentsline {section}{\numberline {12.4}Fakes estimation in the W+jets control region}{138}{section.12.4}%
\contentsline {subsection}{\numberline {12.4.1}Closure Test}{139}{subsection.12.4.1}%
\contentsline {Chapter}{\numberline {13.}\MakeTextUppercase {Control Region Discriminants}}{148}{chapter.13}%
\contentsline {subsection}{\numberline {13.0.1}Gradient Boosted Decision Tree}{149}{subsection.13.0.1}%
\contentsline {subsection}{\numberline {13.0.2}Top WW Discriminant}{150}{subsection.13.0.2}%
\contentsline {subsection}{\numberline {13.0.3}GGF Discriminant}{154}{subsection.13.0.3}%
\contentsline {subsection}{\numberline {13.0.4}VBF Discriminant}{156}{subsection.13.0.4}%
\contentsline {subsection}{\numberline {13.0.5}N-1 BDTs}{160}{subsection.13.0.5}%
\contentsline {Chapter}{\numberline {14.}\MakeTextUppercase {Systematic Uncertainty}}{163}{chapter.14}%
\contentsline {section}{\numberline {14.1}Experimental Uncertainties}{163}{section.14.1}%
\contentsline {subsection}{\numberline {14.1.1}Theory Uncertainty}{164}{subsection.14.1.1}%
\contentsline {subsubsection}{\numberline {14.1.1.1}VBF Uncertainties}{165}{subsubsection.14.1.1.1}%
\contentsline {subsubsection}{\numberline {14.1.1.2}GGF Uncertainties}{167}{subsubsection.14.1.1.2}%
\contentsline {subsubsection}{\numberline {14.1.1.3}Top Uncertainties}{170}{subsubsection.14.1.1.3}%
\contentsline {subsection}{\numberline {14.1.2}WW Uncertainties}{174}{subsection.14.1.2}%
\contentsline {subsubsection}{\numberline {14.1.2.1}Non EWK component}{174}{subsubsection.14.1.2.1}%
\contentsline {subsubsection}{\numberline {14.1.2.2}EWK component}{178}{subsubsection.14.1.2.2}%
\contentsline {subsubsection}{\numberline {14.1.2.3}$Z\rightarrow \tau \tau $ Uncertainties}{184}{subsubsection.14.1.2.3}%
\contentsline {Chapter}{\numberline {15.}\MakeTextUppercase {Unfolding}}{187}{chapter.15}%
\contentsline {section}{\numberline {15.1}Introduction}{187}{section.15.1}%
\contentsline {section}{\numberline {15.2}Iterative Bayesian unfolding}{188}{section.15.2}%
\contentsline {subsection}{\numberline {15.2.1}Statistical Uncertainty}{189}{subsection.15.2.1}%
\contentsline {subsection}{\numberline {15.2.2}Unfolding Bias}{189}{subsection.15.2.2}%
\contentsline {subsection}{\numberline {15.2.3}Coverage}{192}{subsection.15.2.3}%
\contentsline {section}{\numberline {15.3}Tikhonov Regularization}{193}{section.15.3}%
\contentsline {subsection}{\numberline {15.3.1}Bias Tests}{194}{subsection.15.3.1}%
\contentsline {section}{\numberline {15.4}Response Matrices}{195}{section.15.4}%
\contentsline {subsection}{\numberline {15.4.1}Efficiency and Purity}{195}{subsection.15.4.1}%
\contentsline {section}{\numberline {15.5}Fakes and Misses}{200}{section.15.5}%
\contentsline {subsection}{\numberline {15.5.1}Obsevables Binning}{200}{subsection.15.5.1}%
\contentsline {Chapter}{\numberline {16.}\MakeTextUppercase {Statistical Treament}}{207}{chapter.16}%
\contentsline {section}{\numberline {16.1}Introduction}{207}{section.16.1}%
\contentsline {section}{\numberline {16.2}Normalization Parameters}{207}{section.16.2}%
\contentsline {section}{\numberline {16.3}likelihood procedure}{209}{section.16.3}%
\contentsline {section}{\numberline {16.4}statistical fit}{211}{section.16.4}%
\contentsline {subsection}{\numberline {16.4.1}Sample specific uncertanties}{212}{subsection.16.4.1}%
\contentsline {subsection}{\numberline {16.4.2}smoothing}{213}{subsection.16.4.2}%
\contentsline {section}{\numberline {16.5}Asimov fit results}{214}{section.16.5}%
\contentsline {section}{\numberline {16.6}Differential Statistical Analysis}{226}{section.16.6}%
\contentsline {subsection}{\numberline {16.6.1}Bias Test with Madgraph+Herwig signal samples}{227}{subsection.16.6.1}%
\contentsline {subsection}{\numberline {16.6.2}Floating background parameters scheme}{227}{subsection.16.6.2}%
\contentsline {subsection}{\numberline {16.6.3}Asimov Results}{228}{subsection.16.6.3}%
\contentsline {Chapter}{\numberline {17.}\MakeTextUppercase {Results}}{241}{chapter.17}%
\contentsline {subsection}{\numberline {17.0.1}Inclusive Results}{241}{subsection.17.0.1}%
\contentsline {subsection}{\numberline {17.0.2}Differential Results}{242}{subsection.17.0.2}%
\contentsline {Chapter}{\numberline {18.}\MakeTextUppercase {Conlusion}}{259}{chapter.18}%
\contentsline {Chapter}{\MakeTextUppercase {Bibliography}}{261}{chapter*.3}%
