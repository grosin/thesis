One of the poorly understood effects of radiation on 130nm CMOS technology is that when exposed to ionizing radiation, they exhibit an increase in the leakage current, followed by a gradual decrease towards baseline after a peak, making a 'bump' shape. The understanding of this Total Ionizing Dose (TID) induced leakage current is crucial to the design of the ATLAS ITk Strip Detector due to its implication on thermal management and mechanical stability, which relies heavily on the expected power consumption of the modules. Characterizing the TID bump helps determine the safety factor needed for module power consumption \cite{TIDBump} . 

 In the ATLAS detector, the ABCstar chips must withstand dose rates of total ionizing radiation in the range of 0.6 krad$\cdot$h$^{-1}$ - 2.5 krad$\cdot$h$^{-1}$ from LHC collisions and will be operated at sub-zero temperatures. To characterize the effects of TID on the leakage current at typical HL-LHC conditions, the leakage current from four ABCstar chips was measured utilizing the $^{60}$Co gamma source at BNL in a controlled temperature environment. 

Each  ABCStar was read out with a single chip board set inside a container designed for the experiment. The container is insulated with Styrofoam and contained a thermo-electric in thermal contact with the chip which was kept at $-10^{o}$ C for three of the boxes and  $0^{o}$ C for the fourth box. Temperature and humidity were measured with an SHT85 sensor, which is not radiation proof. To protect the sensors, a radiation resistant extension in the box is made to house the sensors while being in environmental contact with the rest of the radiation box. The humidity is kept low by circulating nitrogen through the box and the extension that contains the sensors. I wrote software controls to readout the temperature and humidity sensor through Raspberry pi I2C, control the thermo-electric chiller through serial connection, log the digital and analogue current from the ABCStar and provide software interlock. The analogue and digital current of the ABCStar are measured through the single chip board. The software interlock shuts the power to the chip if humidity persists above ten percent. 

To analyze the results we worked on a simplified physics model of the system. Holes created through radiation migrate to the silicon interface in the transistor and create a path for the current to flow between source and drain that bypasses the gate causing leakage current. The parasitic transistors (trapped holes on the interface) are canceled out by electrons on the opposite side of the silicon oxide interface that are trapped from radiation induced traps in the silicon band gap. Therefore, in our model, the leakage current in these chips is the number of positive states ($n_{h}$) minus the number of negative states  ($n_{e}$) minus the threshold voltage ($n_{thr}$), squared, as shown in equation 1.
\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.6]{figures/radiation_fits.png}
     \caption{\label{fig:Leakage_Current}Digital Leakage current vs. total ionizing dose for ABCStar}
\end{figure*}

\begin{equation}
  \begin{array}{l}
I_{Leakage}=k \cdot (n_{h} - n_{e} - n_{thr} )^{2}  \quad if  \quad n_{h} - n_{e} - n_{thr} >0   \\
I_{Leakage}=0 \quad else
\end{array}
\end{equation}

\begin{equation}
\frac{dn_{h}}{dt}=A_{h} \cdot (N_{h}-n_{h}) - P \cdot n_{h}
\end{equation}
\begin{equation}
\frac{dn_{e}}{dt}=A_{e} \cdot (N_{e}-n_{e}) 
\end{equation}

\begin{equation}
  \begin{array}{l}
I_{Leakage}=k \cdot (\frac{N_{h}A_{h}}{A_{h}+P}(1-e^{-x(A_{h}+P)}) - A_{e}(1-e^{-x\cdot A_{e}}) - n_{thr} )^{2}  \quad if  \quad n_{h} - n_{e} - n_{thr} >0   \\
I_{Leakage}=0 \quad else
\end{array}
\end{equation}

 The differential equations in equation 2 describe the growth in positive and negative states over time. The first equation describes the rate that trapped positive states increase. The first term rate of growth, the rate of hole creation times the number of available hole traps. $N_{h}$ is the number of hole traps at the silicon oxide interface, $n_{h}$ is the number of holes currently trapped, $P_{h}$ is annealing probability and $A_{h}$ is overall normalization. 
	The second term is the annealing term where trapped holes get released due to thermal excitation and quantum tunneling. The second equation for the growth in negative states is the same except without the annealing term. Experiments conducted in Rutherford Appleton Lab (RAL) (figure \ref{fig:anneal}) show that the pre-irradiated chips don’t exhibit the TID bump. The experiments at RAL baked ABC Chips in  the oven at 80 degrees for 3 months then irradiated the chips, shown in the colored dashed lines. As the plot shows, the TID bump did not return, this leads us to believe that negative states don’t anneal. 
	The differential equations for the growth in positive and negative charges is analytically solvable, the solution is shown in equation 4.  The equation has five unknown parameters that we can study through fits to the ABCstar data. The fits and parameters are shown in \ref{fig:Leakage_Current}. The interpretation of the fit parameters is still ongoing. We hope that this data combined with data from RAL will allow us to characterize the curves as functions of temperature and dose rate.
\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.5]{figures/annealing_studies.png}
     \caption{\label{fig:anneal} Results of RAL annealing studies. The bump is the initial TID bump, the other lines show the bump doesn't return}
\end{figure*}

In addition, we looked at the annealing time for positive states. Figure \ref{fig:Fall_Time} shows the decaying current after the source was turned off. Figure \ref{fig:TID_vs_temp} shows the time constant for the exponential fall off as a function of temperature. The time constants is approximately 1-3 days and inversely proportional to temperature, as expected in the model. 

\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.45]{figures/tid_bump_temp.pdf}
     \caption{\label{fig:Fall_Time}Leakage current fall for different temperatures}
\end{figure*}

\begin{figure*}[!htbp]
	\centering
    \includegraphics[scale=0.4]{figures/increase_temp.png}
     \caption{\label{fig:TID_vs_temp}TID Fall time vs. Temperature}
\end{figure*}
