\documentclass[11pt]{article}
%Gummi|065|=)
\usepackage{ amssymb }
\usepackage{amsmath}
\usepackage{graphicx}
\begin{document}

Progress on ATLAS upgrades and analysis of the cross section of Higgs boson to WW decay are presented. Brookhaven National Lab (BNL) will be responsible for  producing about a quarter of the silicon strip modules in the united states, running quality control tests on all US produced modules and assembling staves. Harvard and BNL collaborated on a cold box to conduct QC tests on four modules. The first iteration of the box with the  software framework was successfully tested with four ABC130 modules. Results from ABCStar radiation show the expected bump in the leakage current from  ionizing radiation.

The results of the radiation tests agree with expectations from the physics model. 
Higher dosage leads to more leakage current, because the annealing positive states are quickly replaced with newly created positive states. Higher temperature leads less leakage current, because positive states anneal faster. The Peak of the leakage current occurs at 600 Krad regardless of dosage or temperature. This is true for low dosage, around 1Krad/hour. For high dosage, such as the results from RAL at the order 1 Mrad/hour, the peak is closer 1 Mrad. Calculating a consistent value for the total leakage current as a function of temperature and dosage is difficult because of large batch to batch variations, requiring larger statistics per batch. 

The measurement of the fiducial cross section of a VBF produced Higgs boson decay to WW to  opposite flavor leptons yielded results in agreement with standard model expectations, with approximate 24\%  uncertainty in the final measurement, compared to 50\% uncertainty in the previous measurement. The cross section as a function of thirteen important observables was also  presented, with no significant disagreement with the standard model expectations. 

The VBF production mode is rare, with only 7\% of all the Higgs bosons produced at the LHC. This makes getting large statistics for this production mode difficult.
The uncertainty is still dominated by statistical uncertainty, so the methods developed in this analysis will be an important starting point for future iterations in high luminosity LHC which will benefit from an order of magnitude more data. Theory uncertainty is dominated by the $Z\rightarrow \tau\tau$ process, which will benefit, even possibly in the near future, from increased Monte Carlo statistics. In addition, a combined measurement with the ggF and other Higgs processes will give further possibilities for probing new physics with precision measurements.

The significant backgrounds in the analysis are from standard model $WW$,$t\bar{t}$, Drell Yan, ggF Higgs, and fake leptons. Fake leptons are estimated in a data driven way in the W+Jets control region using the ratio of leptons identified correctly with strict requirements and loose requirements. The other major standard model backgrounds are estimated in the signal region with a statistical fit, aided by multivariat discrimanats. This method resulted in an estimated fiducial cross section of 1.7 $\pm$ 0.42 fb, and 4.7$\sigma$ significance, compared to 1.8$\sigma$ significance of the previous measurement. This agrees with the standard model expectation of 2.07 fb.

The analysis, with increased precision, finds no significant deviations from the standard model. Further reductions in uncertainty are expected soon in run 3 and beyond, both because of higher statistics and increasinly sophisticated usage of new algorithms like multivariate discriminants. 
\end{document} 
