

The Large Hadron Collider (LHC) is a proton-proton storage ring operating at CERN, and for its 9 years of operation, it has been the world’s highest energy particle collider. During LHC operation thus far, protons have collided with increased center-of-mass approaching the design energy of 14 TeV. Instantaneous luminosity has also successively increased, surpassing design instantaneous luminosity of $1024 cm^{-2}s^{-1}$ in 2018 to reach  $1024 cm^{-2}s^{-1}$. The overall data recorded in the ATLAS detector totals more than 1016 collisions.
Operation of the LHC has led to the discovery of the Higgs boson and some of the most precise measurements of its properties including the coupling of the Higgs boson to bottom quarks, W and Z bosons, photons  and tau leptons.
The LHC has also facilitated searches for new physics over a wide parameter space, setting confidence level exclusion limits on masses of supersymmetric particles like squarks, gluinos and neutralinos.
The LHC can run continuously for a few years before detector components need to be repaired and replaced. The schedule of data-taking consists of long periods of data accumulation (Run 1 and Run 2) paired with long shutdown periods. The LHC is set to begin Run 3, in which the design center-of-mass energy should be reached, in 2021. Following Run 3, detector upgrades will be installed during a long shutdown. The High-Luminosity LHC (HL-LHC) will begin colliding protons with ten times the luminosity in 2027.

Acceleration of protons start with the Linac-2. This injects protons at 50 MeV into the Proton Synchronous Booster (PSB) where they are further accelerated to 1.4 GeV. In the Proton Synchrotron (PS), the protons are separated into bunches with a spacing of 25 ns and are further accelerated to 25 GeV before being extracted to the Super Proton Synchrotron (SPS), where they reach 450 GeV. Finally the bunches of protons enters the LHC, where they are accelerated to their final energy of 6.5 TeV. Linac 2, PSB, PS, and SPS were all operational accelerators before the LHC era though each had to be majorly upgraded to handle the energy and beam intensity required for LHC collisions.The LHC layout mimics that of the Large Electron Positron collider (LEP) that was housed in the same tunnels. Figure 3.1 shows the positioning of each experiment at the LHC as well as injection systems and other features. Once proton bunches enter the LHC in two opposing beams, they are accelerated with radio frequency (RF) systems. Located at Point 4 in the LHC schematic, the system consists of 16 RF cavities operating at twice
the frequency of the SPS injector. RF cavities are metallic chambers containing oscillating electromagnetic fields; in the LHC this oscillation frequency is 400 MHz. The tuning of this frequency ensures that protons of the ideal energy are not accelerated further and simply
maintain their momentum while particles arriving in an RF cavity slightly before or after will be decelerated or accelerated toward the ideal proton energy. This acceleration process can also be used to split beams of protons into discrete bunches, and this is first done with RF cavities in the PS. After proton bunches have circled the LHC approximately 1 million times, peak energy is reached and collisions can commence.
\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.55]{figures/LHC_full_diagram.jpg}
     \caption{\label{fig:LHC_diagram} Diagram of the LHC, with the SPS feeding beam. The collision points are where the detectors, ATLAS,CMS,ALICE,LHCb, are setup }
\end{figure*}

\section{The ATLAS detector}
The ATLAS (A Toroidal LHC ApparatuS)  detector diagrammed in \ref{fig:full_atlas_detector} is a large cylindrical detector centered around one of the collision points in the LHC. The detector has many instruments to examine the particles and interactions from the collision. Figure \ref{fig:particle_thru_detector} shows how different standard model particles are detected in each layer ATLAS. 

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.75]{figures/ATLAS_detector_full.png}
     \caption{\label{fig:full_atlas_detector}  The ATLAS detector. The middle of the detector is the proton beam line }
\end{figure*}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.5]{figures/particles_thru_detector.pdf}
     \caption{\label{fig:particle_thru_detector}
     A cross-sectional diagram of the ATLAS detector. The inner most layer, ID, measures tracks of particles. The EM calorimeters causes photons and electrons to shower, the tile calorimeter causes hadrons like protons and neutrons to  shower, which allows us to identify them. Muons reach the Muon Spectrometer in the outer layer. Neutrinos (and potential BSM particles) are invisible to the detector and are simply missing energy}
\end{figure*}


The most sensible coordinate system to describe particles in the ATLAS detector are cylindrical coordinates. The center of the detector, where the protons are most likely to have collided is the interaction point (IP) and is the designated origin of the coordinate system. The distance from the IP in the transverse plane is $d_{0}$ parameter, and $\phi$ is the azimuthal angle around the beam line. The angle in the longitudinal plane is $\theta$. When analyzing data it is more common to look at the pseudorapidity $\eta$, which is defined as  $-ln(tan(\theta/2))$, since we are largely interested in smaller values for the polar angle. The final parameter for detected particles is their momentum $p$, which is calculated from the radius of curvature as they travel through the magnetic field  in the inner detector.  The transverse momentum $p_{T}=p \cdot sin(\theta)$ is often more interesting because the total  momentum of the particles in the transverse plane must be 0, since the protons collide head-on in the direction of the beamline, so therefore the total $p_{T}$ of a single collision must cancel  out. 
\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/inner_detector.jpg}
     \caption{\label{fig:inner_detector} ATLAS Inner Detector}
\end{figure*}
The inner most part of the ATLAS detector is the Inner Detector (ID), which is used to reconstruct the track of charged particles from the IP. It has a coverage of$|\eta| <2.5$ and is bathed in a 2 Telsa solenoida magnetic field. The magnetic field causes the tracks of particles to curve, with the radius of curvature proportional to their momentum. The inner most part of the inner detector, the Insertable-B layer (IBL) added in 2014, consists of four layers silicon pixel detectors. Outside the IBL are the semi-conductor trackers (SCT), which consists of four layers of silicon strip detectors.  The outermost part of the inner detector is the Transition Radiation Tracker (TRT), which consists of 50,000 drift tube detectors in the barrel layer and 250,000 tubes in the endcaps.  



After the Inner Detector are the calorimeters. The first layer of calorimeters are the EM-calorimeters, made of layers of liquid argon and lead. EM-calorimeters measure the energy of electrons and photons. Lead absorbs the particle which coverts them to a shower of low energy particles that ionize the liquid argon. Electrons lose energy through Bremsstrahlung radiation, while photons lose energy from pair production. Surrounding the liquid argon detectors are the hadronic calorimeters(H-Cal). The h-cals are made of layers of steel and plastic scintillators, Hadrons such as protons and neutrons lose energy in the steel and create a shower of particles picked up by the plastic scintillators and measured with photomultiplier tubes. The calorimeters have a coverage of $0<|\eta|<4.9$

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/calorimeters.jpg}
     \caption{\label{fig:calorimeter} EM liquid argon and hadronic tile carlorimeters}
\end{figure*}

Finally the Muon Spectrometer (MS) is outermost layer of ATLAS. Due to their larger mass, muons go through the EM calorimeters without losing much energy to Bremsstrahlung radiation, so are detected in the MS roughly 10m from the IP. The MS consists of three layers of Monitored Drift Tube chambers covering $|\eta| < 2.7$ and an inner layer of Cathode Strip Chambers with
$|\eta| > 2.0$ in the small wheel of the endcap. In addition, it includes trigger chambers that contain 3 layers of Resistive Plate Chambers and 3 layers of Thin Gap Chambers. Muons are a critical measurement in ATLAS, as they can be measured cleanly, with smaller uncertainty and background contamination compared to electrons and photons, providing a useful method of precision measurements.

The ATLAS detector contains a central solenoid magnet, which provides a 2 Tesla magnetic field in the Inner Detector. As discussed, this magnetic field curves the path of charged particles in the ID, allowing us to measure their momentum by the radius of curvature. The inner solenoid magnet is able to achieve a relatively strong field at 4.5 cm thickness using an embedding of niobium-titanium superconductor wires. The MS is embedded in a toroidal magnet, which allows the measurement of the muon's momentum. There are three distinct toroids, one at the barrel and one in each of the two endcaps, reaching a magnetic field of 3.5 Tesla. The magnets operate a temperature of about 4.5$^{0}$ K

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/muon_sepctrometer.jpg}
     \caption{\label{fig:muon_spectrometer} ATLAS muon spectrometer}
\end{figure*}
