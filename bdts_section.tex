 The analysis uses multiple dedicated boosted decision trees (bdt) to isolate each significant process from all other processes. The bdts include a $Z+$jets bdt,$ggF$ bdt and $VBF$ bdt. In addition a bdt is trained to separate Top and WW backgrounds, and a separate bdt to distinguish Top and WW from all other processes. 
 
 In the SR, a dedicated selection requirement is added to further reduce the $Z+$jets background based on the $Z+$jets bdt. For this bdt, several input variables based on the missing transverse momentum are used, and a cut is applied on the bdt output.
 
 A simultaneous fit of SR and CRs to data is carried out to estimate the amount of $t\bar{t}$, DY and $ggF$ background events based on data as well as extract the signal yield. In the fit the signal strengths of the VBF signal, the $t\bar{t}$, $WW$ and $ggF$ processes are left floating, and the signal strength of $t\bar{t}$ is considered in combination with $WW$.  
 For the $ggF$ background estimation, three CRs are used with a dedicated $ggF$ bdt for each region. The $ggF$, $VBF$ and Top WW bdts distributions are used in the simultaneous fit, i.e. no cut is applied on these bdts.
 
 Experimental and theoretical uncertainties are propagated into the simultaneous fit as nuisance parameters and are profiled by the fit.

An overview of the signal, control, and validation regions is shown in Table 1 below. Top and WW validation regions are defined to demonstrate MC modeling of each process in an enriched region, while in the simultaneous fit both of them are estimated directly in the signal region. In Table 2, three $gg$F control regions are defined to estimate $gg$F production Higgs events and a $Z+$jets control region estimates Drell-Yan background in a region orthogonal to the signal region. 

\begin{table}[h!]
\centering
\small{
\begin{tabular}{|l|c|c|c|c|}
\hline
\textbf{VBF SR} & \textbf{$Z+$jets CR} & \textbf{Top VR} & \textbf{$WW$ VR} \\
\hline
2-lepton & 2-lepton & 2-lepton & 2-lepton \\
2-jet (30,30) & 2-jet (30,30) & 2-jet (30,30) & 2-jet (30,30) \\ 
b-veto ($<1$) & b-veto ($<1$) & b-veto ($=1$) & b-veto ($<1$) \\
CJV ($20~GeV$)	& CJV ($20 GeV$) & CJV ($20~GeV$) & CJV ($20~GeV$) \\
OLV bool	 & OLV bool &   OLV bool & $m_T>130$GeV \\
$Ztt$ veto	& $Ztt$ selection & $Ztt$ veto & $mT_{1jet}>160$GeV \\
$M_{jj}$ cut (200$ GeV$) & $M_{jj}$ cut (200$ GeV$) & & \\
$DY_{jj}$ cut (2.1)	& $DY_{jj}$ cut (2.1)  & & \\
bdt $Zjets$ cut ($<0.5$)	& bdt $Zjets$ cut ($>0.5$) & & \\
\hline
\end{tabular}
\caption{Summarized  cuts for VBF signal region, $Z+$jets control region, top validation region, and $WW$ validation region}
\label{tab:allcuts}
}
\end{table}

\begin{table}[h!]
\centering
\small{
\begin{tabular}{|l|c|c|c|c|}
\hline
\textbf{$W+$jets CR} & \textbf{ggF CR1} & \textbf{ggF CR2} & \textbf{ggF CR} \\
\hline
1 ID lepton, 1 anti-ID & 2-lepton & 2-lepton & 2-lepton \\
2-jet (30,30) & 2-jet (30,30) & 2-jet (30,30) & $<2$ jets \\ 
b-veto ($<1$) & b-veto ($<1$) & b-veto ($<1$) & b-veto ($<1$) \\
CJV ($20~GeV$) & CJV$>=20$ and OLV$=1$ \textit{or}  & CJV$>=20$ and OLV$!=1$ &  \\
OLV bool         & CJV$<20$ and OLV$!=$1 & $Ztt$ veto   &  \\
$Ztt$ veto     & $Ztt$ selection & & \\
$M_{jj}$ cut (200$~GeV$) & & & \\
$DY_{jj}$ cut (2.1)     &  & & \\
bdt $Zjets$ cut ($<0.5$) & & & \\
\hline
\end{tabular}
\caption{Summarized  cuts for the $W+$jets control region and ggF control regions (1, 2, 3)}
\label{tab:allcuts2}
}
\end{table}
 
 The signal yield is extracted by multiplying its signal strength by the theoretical cross section and is then corrected for experimental efficiency (C-factor) and integrated luminosity  to finally extract the signal cross section to be compared to theoretical predictions.
 
 \section{Gradient Boosted Decision Tree}
 A decision tree is a collection of successive cuts designed to classify events as signal-like or background-like. A leaf contains all the events left over after all the cuts in a path of the tree are applied. A given signal event is correctly identified if it is placed in a leaf where the majority of events are signal events. and vice-versa for background
events.

 After the initial tree is built another tree is grown to better separate the signal and background
events misidentified by the first tree. This proceeds iteratively until there is a collection of a specified
number of trees, in a process known as boosting. There are two main boosting algorithms, adaptive and gradient boosting. In adaptive boosting the data is reweighed every time a new tree is created so that incorrectly classified data is given more weight in the next training. In the gradient boosting case, the error from the previous iteration is calculated for every data point, and a new  tree is trained to predict the error made by the previous tree. A weighted average based on the learning rate is taken from all these trees to form a
bdt output discriminant with values ranging from -1 to 1, where 1 is signal-like data and -1 in background like-data. The parameters of the bdt tunable by hand,  such as the depth of the tree, total number of cuts and number of trees in the ensemble, are called hyperparamters and the total accuracy is evaluated over a large range of values for each hyperparameter.  

Unlike decision trees, boosted decision trees  (bdt) are robust to overfitting with larger number of trees. Overfitting is when the classifier performs very well on the data used in training but much worse when classifying new data. To check if the classifier overfit, the data is split in half between a training set and testing set, and the hyperparameters of the discriminate is chosen such that the testing set has performance comparable to the training set. 

 %this is from section 10.2 of the supporting notes
 \subsection{Top WW Discriminant}
A bdt is trained using $e\mu + \mu e$ events after the VBF selection and all signal regions cuts so that the phase space in which the bdt is trained is exactly the same as the one where it is applied in the final fit. Since
the top-quark and WW processes are very difficult to disentangle from one another as they share the same final state, the bdt is trained to discriminate top-quark and WW (top+WW) processes against VBF, ggF, Z+jets, and $V\gamma$ events. The MC statistics used in the training are half those available after all signal region cuts, while the other half of the statistics is then used to test the bdt training. This corresponds to about 90,000 un-weighted WW and top events and about 115,000 raw VBF, ggF, Z+jets and $V\gamma$ events. This training is carried out using weighted MC events to best account for overall event distributions and include about 2000
total weighted top and WW events and about 550 weighted events from other processes. The optimal parameters were found through
a scan of reasonable values and the final set is summarized in Table \ref{tab:wwTop_Hyperparameters}.

\begin{table}[h!]
\centering
\small{
\begin{tabular}{|l|l|}
\hline
Parameter &value\\
\hline
Boosting Algorithm & Gradient\\
\hline
Max Tree Depth & 22\\
\hline
Number Of Trees & 200\\
\hline
Minimum number of events per leaf &5\% \\
\hline
Number of cuts & 7\\
\hline
\end{tabular}
\caption{bdt parameters used for the training of the bdt of top + WW vs. other processes.
}
\label{tab:wwTop_Hyperparameters}
}
\end{table}

This bdt utilizes eight lepton and jet kinematic variables to distinguish between signal and background events. These include $\delta Y_{jj}$ , the combination of lepton/jet mass $M_{l0j0}$ for leading lepton and
jet , $\Delta \phi_{ll}$ , $m_{T}$ , $\eta_{j0}$ , $\eta_{j1}$ , $\Delta \phi_{jj}$ , and sum of centralities (L). Plots in figures \ref{fig:topww_bdt_input_vars} and \ref{fig:topww_bdt_input_vars_corr} show the distribution and correlation of input variables.

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.65]{figures/supporting_note/figure66.pdf}
     \caption{\label{fig:topww_bdt_input_vars}Correlations between in the input variables for the singal and background samples for WW+Top bdt }
\end{figure*}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.65]{figures/supporting_note/figure65.pdf}
     \caption{\label{fig:topww_bdt_input_vars_corr}Signal and background spectrums of the input variables to WW+Top bdt }
\end{figure*}

Table ~\ref{tab:wwTop_importance} shows the ranking, i.e. importance, of the variables used as input to the bdt training, and we can
see that $m_{T}$ is dominating.

The bdt training successfully separates top+WW processes from other processes. In order to quantify
the discrimination, an integrated-ROC is calculated through TMVA for weighted normalized samples and
an optimal value of 0.924 is found. Comparisons between the test and training, as seen for instance in
Fig. ~\ref{fig:topww_bdt_overtraining} (left), show that the bdt is un-biased: differences between testing and training distributions of
the bdt would imply overtraining, or that the bdt uses too many parameters on too few events. For signal and background, KS-test values of 0.263 and 0.311 are found, and such values show no evidence of
over-training. The plot in Fig. ~\ref{fig:topww_bdt_overtraining} (right) shows bdt output distribution for all processes in the SR.


\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.45]{figures/supporting_note/figure67.pdf}
     \caption{\label{fig:topww_bdt_overtraining}(Left) comparison of the train and test samples for the WW+Top bdt. The discrepancy between the train and test samples is small indicating theres not overfitting.On the right is the expected data distribution for the bdt broken down by process}
\end{figure*}

\begin{table}[h!]
\centering
\small{
\begin{tabular}{|l|l|}
\hline
Variable &Importance\\
\hline
$m_{T}$ & 0.64\\
\hline
$\Delta Y_{jj}$ & 0.11\\
\hline
$\Delta \phi_{ll}$ & 0.08\\
\hline
$\Delta \phi_{jj}$&0.05\\
\hline
$\Sigma$ Centralities& 0.04\\
\hline
Mass Combination $M_{j0l0}$& 0.04\\
\hline
$\eta_{j1}$ & 0.03\\
\hline
$\eta_{j0}$ & 0.01\\
\hline
\end{tabular}
\caption{ Ranking of the variables used as input to the training of the bdt of top + WW vs. other processes.
}
\label{tab:wwTop_importance}
}
\end{table}
This bdt output distribution is used in the signal region to take advantage of high significance for top+WW
events in the uppermost bins of the distribution. Since this bdt is trained and applied in the signal
region, the modelling of input variables is not directly tested, however, modeling at the pre-selection level for each of these variables as well as in the top validation region show no evidence of any significant
mis-modelling.

\section{GGF Discriminant}
 The multivariate discriminant used for the ggF-CR1 is a bdt trained using
$e\mu + \mu e$ events that pass all ggF-CR1 cuts. The training is for ggF events against VBF signal and all other
backgrounds. Half the MC data is used for training and the other half for testing. This corresponds to about 8,000 ggF events and
around 250,000 events of signal and other background processes. Events are trained with MC weights applied to accurately represent the sample composition, corresponding to 130 weighted ggF events and  10,000 weighted events from other processes. The final set of hyper parameters is summarized in Table \ref{tab:ggf_Hyperparameters}.

\begin{table}[h!]
\centering
\small{
\begin{tabular}{|l|l|}
\hline
Parameter &value\\
\hline
Boosting Algorithm & Gradient\\
\hline
Max Tree Depth & 10\\
\hline
Number Of Trees & 600\\
\hline
Minimum number of events per leaf &5\% \\
\hline
Number of cuts & 7\\
\hline
\end{tabular}
\caption{bdt parameters used for the training of the bdt of ggF vs. other processes.
}
\label{tab:ggf_Hyperparameters}
}
\end{table}
This bdt utilizes eight lepton and jet kinematic variables to distinguish between signal and background events. These include $\delta Y_{ll}$ , $\Delta \phi_{ll}$ , $m_{T}$ , $M_{ll}$ $\Delta \phi_{jj}$, $E^{Miss}_{T}$, Jet $P^{lead}_{T}$ and Jet $P^{sublead}_{T}$ . Plots in figures \ref{fig:ggf_bdt_input_vars} and \ref{fig:ggf_bdt_input_vars_corr} show the distribution and correlation of input variables.

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.65]{figures/supporting_note/figure69.pdf}
     \caption{\label{fig:ggf_bdt_input_vars}Correlations between in the input variables for the singal and background samples for ggF bdt }
\end{figure*}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.65]{figures/supporting_note/figure68.pdf}
     \caption{\label{fig:ggf_bdt_input_vars_corr}Signal and background spectrums of the input variables to ggF bdt }
\end{figure*}


The optimal value of the integrated-ROC calculated is 0.898. Differences between testing and training samples would imply overtraining, or that the bdt uses too many parameters on too few events. The comparison
between the test and training samples in Fig. \ref{fig:ggf_bdt_overtraining} shows that the bdt is un-biased, as we can see that the
testing and trainings samples are very similar. Additionally, a Kolmogorov-Smirnov test is performed to
measure if the test and training distributions differ significantly, and no evidence of over-training is present
(values 0.191, 0.049 for signal, background, respectively).Figure \ref{fig:ggf_bdt_overtraining} shows the signal to background discrimination of the bdt. Finally table \ref{tab:ggf_importance} shows the importance of each feature.

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.45]{figures/supporting_note/figure70.pdf}
     \caption{\label{fig:ggf_bdt_overtraining}(Left) comparison of the train and test samples for the ggF bdt. The discrepency between the train and test samples is small indicating theres not overfitting.On the right is the expected data distribution for the bdt broken down by process}
\end{figure*}

\begin{table}[h!]
\centering
\small{
\begin{tabular}{|l|l|}
\hline
Variable &Importance\\
\hline
$m_{T}$ & 0.25\\
\hline
$\Delta \phi_{ll}$ & 0.24\\
\hline
$\Delta y_{ll}$ & 0.17\\
\hline
$\Delta M_{ll}$&0.16\\
\hline
$\Delta \phi_{jj}$&0.12\\
\hline
$E_{T}^{Miss}$& 0.04\\
\hline
$P^{T}_{j0}$ & 0.00\\
\hline
$P^{T}_{j1}$  & 0.00\\
\hline
\end{tabular}
\caption{  Ranking of the variables used as input to the training of the bdt trained in the ggF Control Region.
}
\label{tab:ggf_importance}
}
\end{table}

%section 11 from supporting note
\section{VBF Discriminant}
The most useful bdt for this analysis is trained to discriminate between the signal process (VBF produced Higgs boson) and the Top and WW process. This bdt is trained using $e\mu + \mu e$ events after the VBF selection and the signal regions cuts including that on $n_{jets}$ , b-veto, OLV, CJV, $M_{jj} > 200$ GeV and $\Delta y_{jj} > 4.5$ . In this way, the phase space in which we train the bdt is broader than the one where we apply it. The training includes only the $t\bar{t}$, $Wt$, $WW$ as well as other
diboson process, as backgrounds, and the VBF signal. The diboson processes are dominated by
WW events. The MC statistics used in the training are half of those available after all signal region cuts (as the other half of the event is then used to test the training). This corresponds to  90,000 un-weighted
WW and top events, and 100,000 raw VBF events. The training includes MC weights on events to best account for the expected process composition. There are  2000 total weighted top and WW events used in the training, and  80 weighted VBF events. The final set of parameters are listed in table \ref{tab:bdt_vbf_hyperparameters}.

\begin{table}[h!]
\centering
\small{
\begin{tabular}{|l|l|}
\hline
Parameter &value\\
\hline
Boosting Algorithm & Gradient\\
\hline
Max Tree Depth & 22\\
\hline
Number Of Trees & 400\\
\hline
Minimum number of events per leaf &5\% \\
\hline
Number of cuts & 7\\
\hline
\end{tabular}
\caption{bdt parameters used for the training of the bdt of VBF vs. other processes.
}
\label{tab:bdt_vbf_hyperparameters}
}
\end{table}

The inut variables to the VBF bdt include $\Delta Y_{ll}$ , $\Delta \phi_{ll}$ , $m_{T}$ , $M_{ll}$, $\Delta \phi_{jj}$, Jet $P^{lead}_{T}$, Jet $P^{sublead}_{T}$, Sum of Centralities, $\Delta Y_{ll}$, $M_{jj}$, $\eta_{j0}$ and $\eta_{j1}$. Figures ~\ref{fig:vbf_bdt_input_vars} and ~\ref{fig:vbf_bdt_input_vars_corr} shows the distribution and correlation of the input features. Figure ~\ref{fig:vbf_bdt_overtraining} shows how well the bdt discriminates between signal and the backgrounds.  The integrated ROC curve has a value of 0.96.  Additionally, a Kolmogorov-Smirnov (KS) test is performed to measure if the the
test and training bdt output distributions differ significantly. If the two distributions are random samples
of the same parent distribution, the KS-test would give a uniformly distributed value between zero and one
(or an average value of 0.5). The closer to 0.5 the KS-test, the greater likelihood the curves come from the
same parent, however this calculation is heavily skewed toward lower values so any value above zero (or not very close to zero, on the order of order $10^{-4}$ ) can be considered not indicative of overtraining. For
signal and background we find KS-test values of 0.107 and 0.154, and so no evidence of over-training. Tabel ~\ref{tab:vbf_bdt_importance} shows the ranking, i.e. the importance of the variables used as input to the VBF vs Top+WW
bdt.
Figure ~\ref{fig:vbf_bdt_signal_to_back_ratio} shows the signal-to-background ratio in the signal region as a function of the bdt used for the.measurement of the inclusive cross section.

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.65]{figures/supporting_note/figure72.pdf}
     \caption{\label{fig:vbf_bdt_input_vars}Correlations between in the input variables for the singal and background samples for vbf bdt }
\end{figure*}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.65]{figures/supporting_note/figure71.pdf}
     \caption{\label{fig:vbf_bdt_input_vars_corr}Signal and background spectrums of the input variables to vbf bdt }
\end{figure*}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.45]{figures/supporting_note/figure73.pdf}
     \caption{\label{fig:vbf_bdt_overtraining}(Left) comparison of the train and test samples for the vbf bdt. The discrepency between the train and test samples is small indicating theres not overfitting.On the right is the expected data distribution for the bdt broken down by process}
\end{figure*}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.75]{figures/supporting_note/figure74.pdf}
     \caption{\label{fig:vbf_bdt_signal_to_back_ratio}Signal to background ratio in each bin of the vbf bdt. The binning is the one used in the inclusive fit: 0.5-0.7,0.7-0.86,0.86-0.94,0.94-1.0}
\end{figure*}

\begin{table}[h!]
\centering
\small{
\begin{tabular}{|l|l|}
\hline
Variable &Importance\\
\hline
$m_{T}$ & 0.14\\
\hline
$\Delta \phi_{ll}$ & 0.12\\
\hline
$\Delta y_{jj}$ & 0.10\\
\hline
$\Delta M_{jj}$&0.09\\
\hline
$\Delta M_{ll}$&0.09\\
\hline
$P^{T}_{j0}$ & 0.08\\
\hline
$\Sigma$ Centralities& 0.08\\
\hline
$\Delta y_{jj}$ & 0.07\\
\hline
$\Delta \eta_{j0}$ & 0.07\\
\hline
$\Delta \eta_{j1}$ & 0.07\\
\hline
$P^{T}_{j1}$  & 0.06\\
\hline
$\Delta \phi_{jj}$&0.05\\
\hline
\end{tabular}
\caption{ Ranking of the variables used as input to the training of the bdt VBF vs Top+WW.
}
\label{tab:vbf_bdt_importance}
}
\end{table}

\section{N-1 Bdts}
For the differential fits, a new bdt is trained for each observable used as an input variable in the original vbf bdt, where that observable, and highly correlated observables, are removed as an input variable.  These N-1 bdts are used as the discriminants in the differential fit in order to not bias the results of the fit. The removed variables have at least 40\% correlation with the observable. Table \ref{tab:N_1_bdt_obs} shows which variables are removed for which N-1 bdt. Figures \ref{fig:vbf_bdt_n_1_dphi} through \ref{fig:vbf_bdt_n_1_jetpt} show the discrimination power of each N-1 bdt. The figures show that the N-1 bdts don't lose significant discriminatory power compared to the full vbf bdt.  

\begin{table}[h!]
\centering
\small{
\begin{tabular}{|l|l|}
\hline
Variable &Removed Variables from vbf bdt\\
\hline
Signed $\Delta \phi_{jj}$ &$|\Delta \phi_{jj}|$ \\
\hline
$\Delta \phi_{ll}$ &$\Delta \phi_{ll}$  and $m_{ll}$\\
\hline
$\Delta y_{jj}$ &$\Delta y_{jj}$  and $m_{jj}$\\
\hline
$\Delta y_{ll}$ &$\Delta y_{ll}$  and $m_{ll}$\\
\hline
$P^{T}_{j0}$ &$P^{T}_{j0}$  and $P^{T}_{j1}$\\
\hline
$P^{T}_{j1}$ &$P^{T}_{j0}$  and $P^{T}_{j1}$\\
\hline
$m_{jj}$ &$\Delta y_{jj}$  and $m_{jj}$\\
\hline
$m_{ll}$ &$\Delta y_{ll}$,$\Delta \phi_{ll}$, and $m_{ll}$\\
\hline
\end{tabular}
\caption{List of N-1 bdts and the removed variables
}
\label{tab:N_1_bdt_obs}
}
\end{table}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.65]{figures/supporting_note/figure75.pdf}
     \caption{\label{fig:vbf_bdt_n_1_dphi}bdt output distribution of test and train data for vbf signal, top+ww background for  $\Delta \phi_{jj}$(left) and  $\Delta \phi_{ll}$(right) }
\end{figure*}


\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.65]{figures/supporting_note/figure76.pdf}
     \caption{\label{fig:vbf_bdt_n_1_dy}bdt output distribution of test and train data for vbf signal, top+ww background for  $\Delta y_{jj}$(left) and  $\Delta y_{ll}$(right) }
\end{figure*}


\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.65]{figures/supporting_note/figure77.pdf}
     \caption{\label{fig:vbf_bdt_n_1_jetpt}bdt output distribution of test and train data for vbf signal, top+ww background for  $\Delta jet pt_{jj}$(left) and  $\Delta m_{ll}$(right) }
\end{figure*}


