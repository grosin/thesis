The uncertainty on the final measurement are the squared sum of uncertainty due statistical fluctuations and uncertainty due to limitations of the detectors or theoretical understanding of the underlying physics. Several sources of systematics are investigated for both the fiducial and the differential cross section measurements. This section discusses the sources of systematic uncertainties and their impacts on the estimated cross sections. 
The sources of systematic uncertainties can be divided into two main categories, experimental and theoretical. Experimental uncertainties are due to the limited abilities of the ATLAS detectors, calibration and reconstruction algorithms. Theoretical uncertainties are from limit the modeling of physics processes. 

\section{Experimental Uncertainties}
Several types of experimental systematics uncertainties affect this analysis. Each of these are calculated for this analysis using the latest recommendations from the Combined Performance (CP) groups, for muons, electrons, jet/E , Trigger, flavor tagging, and pile-up. Table 55 summarizes
the experimental uncertainties that are considered in the analysis.
Here follows a brief description of the sources of experimental systematics, and the corresponding nuisance
parameters used in the analysis, as recommended by the CP groups. The standard set of experimental
systematics is made of 67 four-momentum nuisance parameters and 59 efficiency scale factor nuisance parameters for each muon, electrons and jet uncertainties.

The most significant uncertainties come from the  jet energy resolution, jet energy scale and MET calculations. MET is calculated by taking the negative  sum of all the momentum from the event. The total vector momentum should end up as zero so leftover momentum is  invisible particles such as neutrinos (or new physics) or misreconstructed particles which leads to systematic uncertainty. The jet uncertainty comes from limited resolution of detectors when reconstructing the total energy of the jets. Jet energy scale uncertainty comes from uncertainty in measuring the scale factor to convert detector readings to energy deposits in the calorimeter. 

Jet and lepton uncertainties are evaluated by
testing the number of events in the final state affected by shifting energy/momentum in the MC simulation by a
scale factor. This is done by recalculating the weights applied to MC to match data after shifting the energy/momentum spectrum from the nominal. The shifted spectrum gives the shape and normalization uncertainty. Finally the overall integrated luminosity uncertainty is 1.7\% from the full run 2 dataset.x

The full list of systematic uncertainties are listed in table \ref{tab:expSyst}.

\begin{table*}[h!]
 \centering
\resizebox{\textwidth}{!}{
  \begin{tabular}{l|l}
  \hline\hline
  Systematic uncertainty & Short description \\
  \hline
  \multicolumn{2}{c}{Event}\\
  \hline
  Luminosity         & uncertainty on total integrated luminosity \\
  PRW\_DATASF	     & uncertainty on pileup reweighting          \\
  \hline
  \multicolumn{2}{c}{Electrons}\\
  \hline
  EL\_EFF\_Trigger\_Total\_1NPCOR\_PLUS\_UNCOR            &  trigger efficiency uncertainty          \\
  EL\_EFF\_TriggerEff\_Total\_1NPCOR\_PLUS\_UNCOR         &  trigger efficiency uncertainty          \\
  EL\_EFF\_Reco\_Total\_1NPCOR\_PLUS\_UNCOR               &  reconstruction efficiency uncertainty   \\
  EL\_EFF\_ID\_CorrUncertaintyNP (0 to 15)                &  ID efficiency uncertainty splits in 16 components   \\
  EL\_EFF\_ID\_SIMPLIFIED\_UncorrUncertaintyNP (0 to 17)  &  ID efficiency uncertainty splits in 18 components   \\
  EL\_EFF\_Iso\_Total\_1NPCOR\_PLUS\_UNCOR                &  isolation efficiency uncertainty        \\
  EG\_SCALE\_ALL                                          &energy scale uncertainty \\
  EG\_SCALE\_AF2	                                  &                                          \\
   EG\_RESOLUTION\_ALL                                     &  energy resolution uncertainty           \\
  \hline
  \multicolumn{2}{c}{Muons}\\
  \hline
  MUON\_EFF\_TrigStatUncertainty &  trigger efficiency uncertainty \\ MUON\_EFF\_TrigSystUncertainty & \\                                                 
  MUON\_EFF\_RECO\_STAT                & reconstruction and ID efficiency uncertainty for muons with $p_{T} > 20$ GeV\\
  MUON\_EFF\_RECO\_SYS                 &                                                                      \\
  MUON\_EFF\_RECO\_STAT\_LOWPT         &reconstruction and ID efficiency uncertainty for muons with $p_{T} < 20$ GeV\\
  MUON\_EFF\_RECO\_SYS\_LOWPT         &                                                                      \\
 MUON\_ISO\_STAT                &  isolation efficiency uncertainty                   \\
 MUON\_ISO\_SYS                 &                                                                      \\
 MUON\_TTVA\_STAT               &  track-to-vertex association efficiency uncertainty \\
 MUON\_TTVA\_SYS                &                                                                      \\
  MUON\_ID                       & momentum resolution uncertainty from inner detector                  \\
  MUON\_MS                       & momentum resolution uncertainty from muon system                     \\
  MUON\_SCALE                    & momentum scale uncertainty                                           \\
  MUON\_SAGITTA\_RHO             & charge dependent momentum scale uncertainty         \\
  MUON\_SAGITTA\_RESBIAS         &                                               \\
  \hline
  \multicolumn{2}{c}{Jets}\\
  \hline         
  JET\_EffectiveNP\_Detector (1 to 2) & detector related energy scale uncertainty \\
  JET\_JER\_EffectiveNP\_ (1 to 11) &   \\
  JET\_JER\_EffectiveNP\_12RestTerm & energy resolution uncertainty  \\
  JET\_JER\_DataVsMC\_MC16 	&  \\
  JET\_BJES\_Response & energy scale uncertainty on b-jets \\
  JET\_EffectiveNP\_Mixed (1 to 3) & energy resolution uncertainty \\
  JET\_EffectiveNP\_Modelling (1 to 4) &  energy scale uncertainty on eta-intercalibration (modeling) \\
  JET\_EffectiveNP\_Statistical (1 to 6) &  statistical resolution uncertainty \\
  JET\_EtaIntercalibration\_NonClosure\_highE & \\ 
  JET\_EtaIntercalibration\_NonClosure\_negEta &  energy scale uncertainty on eta-intercalibrations (non-closure) \\
  JET\_EtaIntercalibration\_NonClosure\_posEta & \\
  JET\_EtaIntercalibration\_TotalStat & energy scale uncertainty on eta-intercalibrations (statistics/method) \\
  JET\_EtaIntercalibration\_Modelling & energy scale uncertainty on eta-intercalibrations (modeling)  \\
  JET\_Pileup\_OffsetMu                & energy scale uncertainty on pile-up (mu dependent)    \\
  JET\_Pileup\_OffsetNPV               & energy scale uncertainty on pile-up (NPV dependent)   \\
  JET\_Pileup\_PtTerm                  & energy scale uncertainty on pile-up (pt term)         \\
  JET\_Pileup\_RhoTopology             & energy scale uncertainty on pile-up (density $\rho$)  \\
  JET\_Flavor\_Composition             & energy scale uncertainty on flavour composition       \\
  JET\_Flavor\_Response                & energy scale uncertainty on samples' flavour response \\
  JET\_PunchThrough\_MC16              & energy scale uncertainty for punch-through jets       \\
  JET\_SingleParticle\_HighPt          & energy scale uncertainty from the behaviour of high-$p_{T}$ jets \\
  JET\_JvtEfficiency & JVT efficiency uncertainty      \\
  FT\_EFF\_Eigen\_B (0 to 2) & $b$-tagging efficiency uncertainties (``BTAG\_MEDIUM''): 3 components for $b$ jets, 3 for $c$ jets and 4 for light jets \\
  FT\_EFF\_Eigen\_C (0 to 2)                 & \\
  FT\_EFF\_Eigen\_Light (0 to 3)             & \\
  FT\_EFF\_extrapolation              & $b$-tagging efficiency uncertainty on the extrapolation to high-$p_{T}$ jets\\
  FT\_EFF\_extrapolation\_from\_charm & $b$-tagging efficiency uncertainty on tau jets \\
  \hline
  \multicolumn{2}{c}{MET}\\
  \hline         
  MET\_SoftTrk\_ResoPara & track-based soft term related longitudinal resolution uncertainty \\
  MET\_SoftTrk\_ResoPerp & track-based soft term related transverse resolution uncertainty   \\
  MET\_SoftTrk\_Scale    & track-based soft term related longitudinal scale uncertainty      \\

\hline\hline     
\end{tabular}
}
\caption{Summary of the experimental systematic uncertainties considered.}
\label{tab:expSyst}
\end{table*}

\section{Theory Uncertainty}
Theory systematics are calculated per sample, for Signal VBF, $Z\rightarrow \tau\tau$, top,WW and ggF samples. Theory systematics for the other backgrounds is negligible. The systematics are described in table \ref{tab:all_theory} and in detail in the following section.The most significant uncertainties are estimates of the parton density function (PDF), the strong coupling constant, $\alpha_{s}$ estimates of the QCD scale parameter, matching uncertainty between QCD prediction and parton showers, and generator uncertainties. The uncertainties are evaluated in one of multiple ways. For two point systematics, such as generator uncertainties, an alternate generator is used to calculate the expected yield from the nominal one. The expected uncertainty is then calculated by reflecting the yield from the alternate generator across the nominal one, which gives an envelope of uncertainty. The ranking of the uncertainty impact on the final measurement after the statistical fit is given in the following chapters \cite{Stewart1}.

\begin{table*}[h!]
\tiny
\centering
 \begin{tabular}{l l | cc}
  process    & uncertainty    &variations              &method                \\
             &                &                        &                      \\
  \hline\hline
                           %& \multirow{2}{*}{uncertainty} & Re-       & \multicolumn{3}{c}{Included in fit for} \\\hline\hline
                           %&                              & evaluated & ggF 0/1jet &  VBF   &     ggF 2-jet     \\ \hline\hline
  VBF         & QCD scale      & 7-point                &envelope                         \\
              & PDF            & PDF4LHC15\_nlo\_30\_pdfas(0-30)&symmetric Hessian        \\ 
               &                &        &$\sqrt{\displaystyle\sum_k(X_k-X_0)^2}$   \\
              & $\alpha_s$     & PDF4LHC15\_nlo\_30\_pdfas(31-32)&average                 \\
              & matching       & Powheg+Herwig7, &single variation       \\
              &                & MG5\_aMC@NLO+Herwig7 &                        \\
              & PS/UE          & Powheg+Pythia8,   &single variation        \\
              &                & Powheg+Herwig7 &                        \\
              \hline
              
  ggF         &QCD scale  & 7-point &envelope       \\ 
              & QCD scale      & Stewart-Tackmann     & Stewart-Tackmann       \\ 
              & PDF            & PDF4LHC15\_nlo\_30\_pdfas(0-30)     &symmetric Hessian       \\
              &                &        &$\sqrt{\displaystyle\sum_k(X_k-X_0)^2}$    \\
              & $\alpha_s$     & PDF4LHC15\_nlo\_30\_pdfas(31-32)    &average              \\

              & PS/UE          & Powheg+Pythia8,       &single variation      \\
              &                &Powheg+Herwig7 &                       \\
              \hline

  top      & QCD scale      & 7-point                &envelope                \\
           & PDF            & NNPDF30\_nlo\_as\_0118   &standard deviation      \\
           &                &        &$\sqrt{\frac{1}{N}\displaystyle\sum_{i}(X_i-X_0)^2}$    \\
           & matching       & Powheg+Pythia8,        &single variation    \\
           &                & MG5\_aMC@NLO+Pythia8        &              \\
           & PS/UE          &Powheg+Pythia8,      &single variation       \\
           &                & Powheg+Herwig7 &                       \\
           & ISR            & Var3cUp, Var3cDown    &up down variation  \\
           & FSR            & muRfac$=2.0$, muRfac$=0.5$&up down variation  \\
           %& radiation (ISR/FSR)          & Yes       &    Yes     &  Yes           \\
        
           & Top/$Wt$ interference  & DS, DR scheme &single variation \\
           \hline
              
  $WW$        & QCD scale      & 7-point                &envelope                \\
  (reco       & PDF            & NNPDF30\_nnlo\_as\_0118  &standard deviation       \\ 
  -level)     &                &        &$\sqrt{\frac{1}{N}\displaystyle\sum_{i}(X_i-X_0)^2}$    \\
              & $\alpha_s$     & NNPDF30\_nnlo\_as\_0117, &up down  variation        \\
              &                & NNPDF30\_nnlo\_as\_0119  &                        \\\hline
              
  $WW$        & merging        & ckkw15, ckkw30         &     up down variation                \\
 (truth       & resummation    & QSF down, QSF up       &     up down variation                \\
 -level)      & PS/UE          & Powheg+Pythia8,        &     single variation               \\
              &                & Powheg+Herwig++    &                       \\
              & PS recoil scheme& CSSKIN           &     single variation                \\
              \hline
             % & $gg \rightarrow WW$ uncertainty & Yes        &   Yes    & Yes         \\
             % & EW correction                & No        &   tbi    &  No          \\ 
              
  
  Z$\tau\tau$   & QCD scale      & 7-point                &envelope                \\
              & PDF            & NNPDF30\_nnlo\_as\_0118  &standard deviation       \\ 
              &                &        &$\sqrt{\frac{1}{N}\displaystyle\sum_{i}(X_i-X_0)^2}$    \\
              & $\alpha_s$     & NNPDF30\_nnlo\_as\_0117,  &up down  variation          \\
              &                & NNPDF30\_nnlo\_as\_0119   &                        \\
              & generator      & Sherpa2.2.1+Pythia8,        &single variation    \\
              &                & MG5\_aMC@NLO+Pythia8        &              \\\hline

  \hline\hline
 \end{tabular}
\caption{A summary of theory systematics of all processes, including the types of theory uncertainty and their calculation methods. }
\label{tab:all_theory}
\end{table*}

\subsubsection{VBF Uncertainties}
The VBF theoretical uncertainties include parton shower (PS), PDF, $\alpha_{s}$ , QCD scale variations and matching
scheme. Parton shower uncertainties are derived from comparison between different parton shower generators ( Powheg+Pythia8 and Powheg+Herwig7) and matching uncertainties from the comparison between
different matrix element simulation ( Powheg+Herwig7 and Madgraph5+Herwig7).
These uncertainties constitute the largest theoretical uncertainties in the signal region.
The variation is obtained by internal re-weighting within the generation of the nominal Powheg+Pythia8 sample, which is then used to calculate for the PDF, $\alpha_{s}$ and QCD scale uncertainties. 
The $\alpha_{s}$  uncertainties are accounted for by varying the $\alpha_{s}$  value by ±0.001 with respect to its central
value of 0.118. QCD scale uncertainties include variations of factorization and renormalization scales by
factors of 2 and 0.5, and are found with an 7-point envelope scheme, in which unphysical diagonal terms,
e.g. renormalization scale multiplied by 2.0 and factorization scale multiplied by 0.5, are not considered.

systematics and their impact in signal yields in the signal region, control/validation regions are shown in
Table \ref{tab:vbf_systematics_impact}.

These uncertainties are examined in Figs. \ref{fig:vbf_theory_uncert_spectrum_SR2} (signal region 2) and \ref{fig:vbf_theory_uncert_spectrum_SR1} (signal region 1) for any shape effects on the BDT output used in the
final fit. The plots in the signal region show very little shape effects.

\begin{table*}[!htbp]
        \centering
    \includegraphics[scale=0.75]{figures/supporting_note/table57.pdf}
     \caption{\label{tab:vbf_systematics_impact} VBF theory uncertainties impact on VBF yields in the signal,control and validation regions}
\end{table*}


\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/figure99.pdf}
     \caption{\label{fig:vbf_theory_uncert_spectrum_SR2}Up and down variations on the VBF bdt output distribution for the considered theoretical
uncertainties on the VBF signal. The nominal sample is shown in black and slopes are calculated for up and down
variations to display any potential linear effect on shapes. The yellow band in the ratio plot shows the MC stat
uncertainty of the nominal sample. Distributions are shown after all signal region cuts and $DVBF >0.5$.
}
\end{figure*}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/figure100.pdf}
     \caption{\label{fig:vbf_theory_uncert_spectrum_SR1}Up and down variations on the top +WW vs All BDT output distribution for the considered theoretical
uncertainties on the VBF signal. The nominal sample is shown in black and slopes are calculated for up and down
variations to display any potential linear effect on shapes. The yellow band in the ratio plot shows the MC stat
uncertainty of the nominal sample. Distributions are shown after all signal region cuts and $DVBF <0.5$.
}
\end{figure*}
\subsubsection{GGF Uncertainties}
The ggF parton shower uncertainties use the same prescription described for the VBF samples.
The ggF theoretical uncertainties also include PDF, and $\alpha_{s}$ . PDF and $\alpha_{s}$  uncertainties are derived with the
same strategy as the VBF process. The $\alpha_{s}$  uncertainties are accounted for by varying the $\alpha_{s}$  value by
±0.001 with respect to its central value of 0.118.

For the evaluation of the QCD scale uncertainty, the Stewart-Tackmann (ST) method is used. The
uncertainty calculated from this method is reported in Table \ref{tab:ggf_systematics_impact}. Since the ST method depends on a certain
spectrum of the Higgs transverse momentum and on the transverse momentum of the Higgs plus leading two jets system for jet multiplicity greater than one, the central jet veto (CJV) might modify the spectrum
significantly. The shape difference of transverse momentum of the Higgs plus two leading jets system
is studied before and after the cut associated with the third jet in figure \ref{fig:pt_higgs_thrid_jet_cut} The difference is small and therefore we conclude the ggF control region phase space definition does not introduce large extrapolation
and the ST method is valid for QCD scale uncertainty evaluation.
The BDT output of VBF and ggF-CR1 of the uncertainties used in the fit are shown in figure \ref{fig:ggf_theory_uncert_spectrum_SR1} and
figure \ref{fig:ggf_theory_uncert_spectrum_SR2}

\begin{table*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/table58.pdf}
     \caption{\label{tab:ggf_systematics_impact} ggF theory uncertainties impact on ggF yields in the signal,control and validation regions}
\end{table*}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/figure101.pdf}
     \caption{\label{fig:pt_higgs_thrid_jet_cut} The shape difference of transverse momentum of the Higgs plus two leading jets system before and after
the cut associated with a third jet.
}
\end{figure*}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/figure103.pdf}
     \caption{\label{fig:ggf_theory_uncert_spectrum_SR1}Up and down variations on the VBF BDT output distribution for the considered theoretical
uncertainties on the ggF spectrum. The nominal sample is shown in black and slopes are calculated for up and down
variations to display any potential linear effect on shapes. The yellow band in the ratio plot shows the MC stat
uncertainty of the nominal sample. Distributions are shown after all signal region cuts and $DVBF >0.5$.
}
\end{figure*}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/figure104.pdf}
     \caption{\label{fig:ggf_theory_uncert_spectrum_SR2}Up and down variations on the top +WW vs All BDT output distribution for the considered theoretical
uncertainties on the  ggF spectrum. The nominal sample is shown in black and slopes are calculated for up and down
variations to display any potential linear effect on shapes. The yellow band in the ratio plot shows the MC stat
uncertainty of the nominal sample. Distributions are shown after all signal region cuts and $DVBF <0.5$.
}
\end{figure*}

\subsection{Top Uncertainties}
The top background consists of $t\bar{t}$ and $Wt$ (single-top) processes where $t\bar{t}$  is the dominant background by far. Theoretical
uncertainties are produced for both single top and $t\bar{t}$  samples independently and both include those from
a variety of sources including hard scatter generation, parton shower, factorization and renormalization
scales, initial state radiation (ISR), final state radiation (FSR), and PDFs. For parton shower uncertainties,
the same scheme used in ggF and VBF samples is used (i.e. comparisons between Powheg+Pythia8 and
Powheg+Herwig7). Hard scatter matching uncertainties are similarly derived from comparing the nominal
generation with the one by Madgraph5 as alternative generator. QCD scale uncertainties
from the 7-point envelope of renormalization and factorization variations. ISR and FSR uncertainties
are determined through an up and down variation weights from Pythia8. PDF uncertainty is from the
NNPDF30\_nlo variations in the Powheg+Pythia8 sample.
The $t\bar{t}$  sample does not include NNLO corrections since the
normalization of the background is derived in the fit to data, and the application of such reweighing shows
spurious effects on the top modeling, specifically in the MC-matching uncertainty, in SR and Top VR.
The Wt events contain one additional systematic uncertainty - interference. This is derived from removing
$t\bar{t}$ -like contributions at the level of amplitude (DR) or matrix elements (DS). Table \ref{tab:ttbar_systematics_impact} shows theory
uncertainty from $t\bar{t}$  events in the signal, control and validation regions while Table \ref{tab:single_top_systematics_impact} shows the same for
Wt. While Wt uncertainties are large, the background has a minimal contribution to the analysis so these
do not have any large effect on overall results.

\begin{table*}[!htbp]
        \centering
    \includegraphics[scale=0.45]{figures/supporting_note/table59.pdf}
     \caption{\label{tab:ttbar_systematics_impact} top theory uncertainties impact on $t\bar{t}$ yields in the signal,control and validation regions}
\end{table*}

\begin{table*}[!htbp]
        \centering
    \includegraphics[scale=0.45]{figures/supporting_note/table60.pdf}
     \caption{\label{tab:single_top_systematics_impact} top theory uncertainties impact on single top yields in the signal, control and validation regions}
\end{table*}

These uncertainties are examined in Fig. \ref{fig:single_top_theory_uncert_spectrum_SR2} for any shape effects on the BDT output used in the final fit.
The plots show very little statistics to discern any shape effects.
Figure \ref{fig:ttbar_theory_uncert_spectrum_SR2} shows very little shape effects from theoretical uncertainties on the far more numerous $t\bar{t}$ 
backgrounds. Figure \ref{fig:ttbar_theory_uncert_spectrum_SR1} shows some shape effects from theoretical uncertainties in the part of the Signal
Region not very populated by $t\bar{t}$  backgrounds.

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/figure105.pdf}
     \caption{\label{fig:single_top_theory_uncert_spectrum_SR2}Up and down variations on the topWW bdt output distribution for the considered theoretical
uncertainties on the single top spectrum. The nominal sample is shown in black and slopes are calculated for up and down
variations to display any potential linear effect on shapes. The yellow band in the ratio plot shows the MC stat
uncertainty of the nominal sample. Distributions are shown after all signal region cuts and $DVBF <0.5$.
}
\end{figure*}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/figure106.pdf}
     \caption{\label{fig:ttbar_theory_uncert_spectrum_SR2}Up and down variations on the top +WW vs All BDT output distribution for the considered theoretical
uncertainties on the  $t\bar{t}$ spectrum. The nominal sample is shown in black and slopes are calculated for up and down
variations to display any potential linear effect on shapes. The yellow band in the ratio plot shows the MC stat
uncertainty of the nominal sample. Distributions are shown after all signal region cuts and $DVBF <0.5$.
}
\end{figure*}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/figure107.pdf}
     \caption{\label{fig:ttbar_theory_uncert_spectrum_SR1}Up and down variations on the VBF BDT output distribution for the considered theoretical
uncertainties on the  $t\bar{t}$ spectrum. The nominal sample is shown in black and slopes are calculated for up and down
variations to display any potential linear effect on shapes. The yellow band in the ratio plot shows the MC stat
uncertainty of the nominal sample. Distributions are shown after all signal region cuts and $DVBF >0.5$.
}
\end{figure*}
\subsection{WW Uncertainties}
\subsubsection{Non EWK component}
Some of the WW background theoretical uncertainties are derived from reco-level samples: PDF, $\alpha_{s}$ , and
QCD scale variations; others are derived from truth-level samples: merging, resummation, parton shower
and recoil scheme uncertainties. In the latter case, the samples are only available at truth-level, so the
full event selections was replicated at truth-level. For the most part, this is accomplished by replacing
the variables used in the event selection with their truth-level equivalents. 
PDF and $\alpha_{s}$ uncertainties are derived from the NNPDF30\_nnlo MC replica sets and QCD uncertainties
from the 7-point scale variations envelope.

From the truth-level samples, parton shower uncertainty is derived from a comparison between Powheg+Pythia8
and Powheg+Herwig++. Uncertainty from merging scale is calculated from samples with CKKW merging
scale at 15 GeV and 30 GeV, and resummation uncertainty from samples with resummation scale of parton
shower multiplied by 0.5 and 2.0. Alternative parton shower recoil scheme (CSSKIN) is used to generate
the recoil scheme uncertainty.
The WW theory systematics within the signal region, control/validation regions are shown in Table \ref{tab:reco_ww_systematics_impact} and
\ref{tab:truth_ww_systematics_impact}.These uncertainties are examined in Figs. \ref{fig:ww_theory_uncert_spectrum_SR2} and \ref{fig:ww_theory_uncert_spectrum_SR1} for any shape effects on the BDT output used in the
final fit. The plots show no shape effects within MC stat uncertainty.


\begin{table*}[!htbp]
        \centering
    \includegraphics[scale=0.45]{figures/supporting_note/table61.pdf}
     \caption{\label{tab:reco_ww_systematics_impact} Breakdown of WW theory uncertainties derived from reco-level samples and their impact on the WW
yields in SR and CRs/VRs.
}
\end{table*}


\begin{table*}[!htbp]
        \centering
    \includegraphics[scale=0.45]{figures/supporting_note/table62.pdf}
     \caption{\label{tab:truth_ww_systematics_impact} Breakdown of WW theory uncertainties derived from truth-level samples and their impact on the WW
yields in SR and CRs/VRs.}
\end{table*}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/figure108.pdf}
     \caption{\label{fig:ww_theory_uncert_spectrum_SR2}Up and down variations on the top +WW vs All BDT output distribution for the considered theoretical
uncertainties on the  WW spectrum. The nominal sample is shown in black and slopes are calculated for up and down
variations to display any potential linear effect on shapes. The yellow band in the ratio plot shows the MC stat
uncertainty of the nominal sample. Distributions are shown after all signal region cuts and $DVBF <0.5$.
}
\end{figure*}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/figure109.pdf}
     \caption{\label{fig:ww_theory_uncert_spectrum_SR1}Up and down variations on the VBF BDT output distribution for the considered theoretical
uncertainties on the WW spectrum. The nominal sample is shown in black and slopes are calculated for up and down
variations to display any potential linear effect on shapes. The yellow band in the ratio plot shows the MC stat
uncertainty of the nominal sample. Distributions are shown after all signal region cuts and $DVBF >0.5$.
}
\end{figure*}
\subsubsection{EWK component}
Theoretical uncertainties on electroweak WW background include PDF, $\alpha_{s}$, and
QCD scale variation. Additionally, the parton shower uncertainty is derived from a comparison between
Madgraph5+Pythia8 and Madgraph5+Herwig7.
The effect of PDF uncertainty was evaluated using the standard deviation of 100 NNPDF replicas around
the nominal. Similarly, the effect of  $\alpha_{s}$ was evaluated using three replicas with different settings of  $\alpha_{s}$ .
A normalization uncertainty of ±15\% is applied in all regions as the EW scale uncertainty. The main effect
of missing EW scales is expected to come from the NLO EW correction. This correction has not yet been
computed for the opposite-sign EW WW process. However, it stems from logarithmic corrections that are
very consistent across VB samples. As such, the so-called “leading-log approximation” can be used to
derive an estimate of its size. This approximation has been used in previous analysis, for which the full NLO EW corrections have also been derived. It generally gives a result
that agrees within 1-2\% with the full calculation. More details of the calculation are given in the reference. %reference
The EW WW theory systematics within the signal region, control/validation regions are shown in Table \ref{tab:EWK_ww_systematics_impact}.

These uncertainties are examined in Figs. \ref{fig:ww_uncert1}, \ref{fig:ww_uncert3}, and. \ref{fig:ww_uncert3} for any shape effects on the BDT output used
in the final fit. The plots show no significant shape effects within MC stat uncertainty.


\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/figure107.pdf}
     \caption{\label{fig:ww_uncert1}Up and down variations on the top+WW vs All BDT output distribution for the considered theoretical
uncertainties on the diboson spectrum. The nominal sample is shown in black and slopes are calculated for up and down
variations to display any potential linear effect on shapes. The yellow band in the ratio plot shows the MC stat
uncertainty of the nominal sample. Distributions are shown after all signal region cuts and $DVBF <0.5$.
}
\end{figure*}


\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/figure108.pdf}
     \caption{\label{fig:ww_uncert2}Up and down variations on the top+WW vs All BDT output distribution for the considered theoretical
uncertainties on the diboson spectrum. The nominal sample is shown in black and slopes are calculated for up and down
variations to display any potential linear effect on shapes. The yellow band in the ratio plot shows the MC stat
uncertainty of the nominal sample. Distributions are shown after all signal region cuts and $DVBF <0.5$.
}
\end{figure*}


\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/figure109.pdf}
     \caption{\label{fig:ww_uncert3}Up and down variations on the top +WW vs All BDT output distribution for the considered theoretical
uncertainties on the diboson spectrum. The nominal sample is shown in black and slopes are calculated for up and down
variations to display any potential linear effect on shapes. The yellow band in the ratio plot shows the MC stat
uncertainty of the nominal sample. Distributions are shown after all signal region cuts and $DVBF <0.5$.
}
\end{figure*}


\begin{table*}[!htbp]
        \centering
    \includegraphics[scale=0.45]{figures/supporting_note/table63.pdf}
     \caption{\label{tab:EWK_ww_systematics_impact} Breakdown of EWK WW theory uncertainties their impact on the WW
yields in SR and CRs/VRs.}
\end{table*}

\subsection{$Z\rightarrow \tau\tau$ Uncertainties}
The  $Z\rightarrow \tau\tau$ uncertainties include PDF, $\alpha_{s}$ , QCD scale, and generator variations. PDF and $\alpha_{s}$
uncertainties are calculated from internal re-weighting within the nominal Sherpa  samples using the
NNPDF30\_nnlo PDF sets. QCD uncertainties include the 7-point variation of renormalisation and factorization scales. Generator uncertainties are derived from a comparison of the baseline generator Sherpa 
with Madgraph5, both interfaced with Pythia8. The Madgraph5 samples
suffer from low statistics manifested as large shape uncertainties. The $Z\rightarrow \tau\tau$  theory systematics within
the signal region, control/validation regions are shown in Table \ref{tab:ztt_systematics_impact}.
These uncertainties are examined in Figs. \ref{fig:ztt_theory_uncert_spectrum_SR2} and. \ref{fig:ztt_uncert_spectrum_SR1} for any shape effects on the BDT output used in the
final fit.

\begin{table*}[!htbp]
        \centering
    \includegraphics[scale=0.43]{figures/supporting_note/table64.pdf}
     \caption{\label{tab:ztt_systematics_impact} Breakdown of $Z\rightarrow\tau\tau$ theory uncertainties their impact on the Zjets
yields in SR and CRs/VRs.}
\end{table*}

\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/figure110.pdf}
     \caption{\label{fig:ztt_theory_uncert_spectrum_SR2}Up and down variations on the top +WW vs All BDT output distribution for the considered theoretical
uncertainties on the $Z\rightarrow\tau\tau$ spectrum. The nominal sample is shown in black and slopes are calculated for up and down
variations to display any potential linear effect on shapes. The yellow band in the ratio plot shows the MC stat
uncertainty of the nominal sample. Distributions are shown after all signal region cuts and $DVBF <0.5$.
}
\end{figure*}


\begin{figure*}[!htbp]
        \centering
    \includegraphics[scale=0.35]{figures/supporting_note/figure111.pdf}
     \caption{\label{fig:ztt_uncert_spectrum_SR1}Up and down variations on the ggF BDT output distribution for the considered theoretical
uncertainties on the $Z\rightarrow\tau\tau$  spectrum. The nominal sample is shown in black and slopes are calculated for up and down
variations to display any potential linear effect on shapes. The yellow band in the ratio plot shows the MC stat
uncertainty of the nominal sample. Distributions are shown in the ggF control region.
}
\end{figure*}


